app.controller('mainController', [
    '$scope', '$http', '$timeout', '$window','$compile',
    function mainController($scope, $http, $timeout, $window, $compile) {
        $scope.resting = true;
        $scope.output = {};
        $scope.formData = {};
        $scope.personalitySwitch = true;
        $scope.careers = [];
		$scope.temp = [];
        $scope.categoryfilter = "";
        $scope.cFiltersArray = [];
		$scope.sortData = "original-order";
        $scope.demandLimit = 3;
		$scope.car = [];
		$scope.animationActive = false;
        $scope.loggedin = false;
        $scope.outlookfilter = "";
        $scope.activeFilter  = false;
        $scope.initialLoad = true;
        $scope.itemsCount = 0;
        $scope.tempArray = [];
        $scope.IsotopeOptions = {
          itemSelector: '.element-item',
          layoutMode: 'fitRows',
          fitRows: {
          gutter: 0
          },
  visibleStyle: { opacity: 1 },
  hiddenStyle: { opacity: 0 },
  filter: function() { 
        var items = $(this);
          
        if ($scope.cFiltersArray.length &&  $scope.demandFiltersArray.length){
           $scope.itemsCount++;
          return $scope.cFiltersArray.find(k => items.attr('data-category') == k) && $scope.demandFiltersArray.find(j => items.attr('data-outlook') == j)
        }
        else if ($scope.cFiltersArray.length){
         $scope.itemsCount++;
           return $scope.cFiltersArray.find(k => items.attr('data-category') == k);
        }
        else if ($scope.demandFiltersArray.length) {
           $scope.itemsCount++;
          return $scope.demandFiltersArray.find(j => items.attr('data-outlook') == j)
        }
        else if ($scope.tempArray.length){
          return $scope.tempArray.find(k => items.attr('name') == k);
        }
        else{
          
          if ($scope.itemsCount >= $scope.careers.length) { $scope.itemsCount = 0; }
            return items;
          
        }
  },
  getSortData: {
    name: '.name',
    symbol: '.symbol',
    number: '.number parseInt',
    category: '[data-category]',
    entryLevel :  function(itemElem) {
              if($( "#selectCity" ).val() == '[toronto]') return  $( itemElem ).attr('toronto_low');
              if($( "#selectCity" ).val() == '[ottawa]') return  $( itemElem ).attr('ottawa_low');
              if($( "#selectCity" ).val() ==  'london') return  $( itemElem ).attr('london_low');
              if($( "#selectCity" ).val() ==  'hamilton_niagra') return  $( itemElem ).attr('hamilton_niagra_low');
              if($( "#selectCity" ).val() ==  'kingston_pembrooke') return  $( itemElem ).attr('kingston_pembrooke_low');
              if($( "#selectCity" ).val() ==  'kitchener_waterloo_barrie') return  $( itemElem ).attr('kitchener_waterloo_barrie_low');
              if($( "#selectCity" ).val() ==  'muskoka_kawartha') return  $( itemElem ).attr('muskoka_kawartha_low');
              if($( "#selectCity" ).val() ==  'northeast') return  $( itemElem ).attr('northeast_low');
              if($( "#selectCity" ).val() ==  'northwest') return  $( itemElem ).attr('northwest_low');
              if($( "#selectCity" ).val() ==  'stratford_bruce_peninsula') return  $( itemElem ).attr('stratford_bruce_peninsula_low');
              if($( "#selectCity" ).val() ==  'windsor_sarnia') return  $( itemElem ).attr('windsor_sarnia_low')  ; 
    }, 
    seniorLevel: function(itemElem) {
              if($( "#selectCity" ).val() == '[toronto]') {return  $( itemElem ).attr('toronto_high');}
              if($( "#selectCity" ).val() ==  '[ottawa]') {return  $( itemElem ).attr('ottawa_high');}
              if($( "#selectCity" ).val() == 'london') return  $( itemElem ).attr('london_high');
              if($( "#selectCity" ).val() ==  'hamilton_niagra') return  $( itemElem ).attr('hamilton_niagra_high');
              if($( "#selectCity" ).val() ==  'kingston_pembrooke') return  $( itemElem ).attr('kingston_pembrooke_high');
              if($( "#selectCity" ).val() ==  'kitchener_waterloo_barrie') return  $( itemElem ).attr('kitchener_waterloo_barrie_high');
              if($( "#selectCity" ).val() ==  'muskoka_kawartha') return  $( itemElem ).attr('muskoka_kawartha_high');
              if($( "#selectCity" ).val() ==  'northeast') return  $( itemElem ).attr('northeast_high');
              if($( "#selectCity" ).val() ==  'northwest') return  $( itemElem ).attr('northwest_high');
              if($( "#selectCity" ).val() ==  'stratford_bruce_peninsula') return  $( itemElem ).attr('stratford_bruce_peninsula_high');
              if($( "#selectCity" ).val() ==  'windsor_sarnia') return  $( itemElem ).attr('windsor_sarnia_high');
    }, 
    weight: function( itemElem ) {
      var weight = $( itemElem ).find('.weight').text();
      return parseFloat( weight.replace( /[\(\)]/g, '') );
    }
  }
};
        $grid = $('.grid').isotope($scope.IsotopeOptions);
        $scope.careerdemandArray = {
            0: 'No Data Available',
            3: 'High Demand Career',
            2: 'Average Demand Career',
            1: 'Low Demand Career'
        };
        $scope.demandFiltersArray = [];
        $scope.personalities = [
            [{ "code": "INTJ", "name": "Architect", "color": "green", }, { "code": "INTP", "name": "Logician", "color": "green" }, { "code": "ENTJ", "name": "Commander", "color": "green" }, { "code": "ENTP", "name": "Debater", "color": "green" }],
            [{ "code": "INFJ", "name": "Advocate", "color": "blue" }, { "code": "INFP", "name": "Mediator", "color": "blue" }, { "code": "ENFJ", "name": "Protagonist", "color": "blue" }, { "code": "ENFP", "name": "Campaigner", "color": "blue" }],
            [{ "code": "ISTJ", "name": "Logistician", "color": "orange" }, { "code": "ISFJ", "name": "Defender", "color": "orange" }, { "code": "ESTJ", "name": "Executive", "color": "orange" }, { "code": "ESFJ", "name": "Consul", "color": "orange" }],
            [{ "code": "ISTP", "name": "Virtuoso", "color": "red" }, { "code": "ISFP", "name": "Adventurer", "color": "red" }, { "code": "ESTP", "name": "Entrepreneur", "color": "red" }, { "code": "ESFP", "name": "Entertainer", "color": "red" }]
        ];
        $scope.careerBtn = false;
        $scope.selectedCareers = [];
        $scope.personality = '';
        $scope.searchPrograms = '';
        $scope.Math = window.Math;
        //  $scope.feedback = {};

        // automatically get all courses
        $http.get('/api/get-courses/').then(function(response) {
            $scope.allcourses = response.data;
        });


        if (navigator.userAgent.match(/Android/i) ||
            navigator.userAgent.match(/webOS/i) ||
            navigator.userAgent.match(/iPhone/i) ||
            navigator.userAgent.match(/iPad/i) ||
            navigator.userAgent.match(/iPod/i) ||
            navigator.userAgent.match(/BlackBerry/i) ||
            navigator.userAgent.match(/Windows Phone/i)
        ) {
            $scope.careerLimit = 10;
        } else {
            $scope.careerLimit = 28;
        }

        $scope.getSessionData = function() {
            console.log("called getSessionData");

            $http.get('/api/get-session-data/').then(function(response) {
                $scope.output.num_programs = response.data.programs;
                $scope.output.num_careers = response.data.careers;
                $scope.output.salary = response.data.salary;
                $scope.formData.grade1 = response.data.grade1;
                $scope.formData.grade2 = response.data.grade2;
                $scope.formData.grade3 = response.data.grade3;
                $scope.formData.grade4 = response.data.grade4;
                $scope.formData.grade5 = response.data.grade5;
                $scope.formData.grade6 = response.data.grade6;
                $scope.formData.grade7 = response.data.grade7;
                $scope.formData.grade8 = response.data.grade8;
                $scope.formData.course1 = response.data.course1;
                $scope.formData.course2 = response.data.course2;
                $scope.formData.course3 = response.data.course3;
                $scope.formData.course4 = response.data.course4;
                $scope.formData.course5 = response.data.course5;
                $scope.formData.course6 = response.data.course6;
                $scope.formData.course7 = response.data.course7;
                $scope.formData.course8 = response.data.course8;
            });

        }

        $scope.getUserData = function() {
            console.log("called getUserData");

            $http.get('/api/get-user-data/').then(function(response) {
                $scope.output.num_programs = response.data.summary_programs;
                $scope.output.num_careers = response.data.summary_careers;
                $scope.output.salary = response.data.summary_salary;
                $scope.formData.grade1 = response.data.grade1;
                $scope.formData.grade2 = response.data.grade2;
                $scope.formData.grade3 = response.data.grade3;
                $scope.formData.grade4 = response.data.grade4;
                $scope.formData.grade5 = response.data.grade5;
                $scope.formData.grade6 = response.data.grade6;
                $scope.formData.grade7 = response.data.grade7;
                $scope.formData.grade8 = response.data.grade8;
                $scope.formData.course1 = response.data.course1;
                $scope.formData.course2 = response.data.course2;
                $scope.formData.course3 = response.data.course3;
                $scope.formData.course4 = response.data.course4;
                $scope.formData.course5 = response.data.course5;
                $scope.formData.course6 = response.data.course6;
                $scope.formData.course7 = response.data.course7;
                $scope.formData.course8 = response.data.course8;
            });

        }


        $scope.checkCourse = function(num) {

            //console.log($scope.formData);

            if (!$scope.formData.grade1 && !$scope.formData.course2 && !$scope.formData.course3 && !$scope.formData.course4 && !$scope.formData.course5 && !$scope.formData.course6 && !$scope.formData.course7 && !$scope.formData.course8) {
                $scope.resting = true;
                $scope.noresults_message = false;
                $scope.output.num_programs = -1;
                $scope.output.num_careers = -1;
                $scope.output.salary = -1;
            }

            //console.log(this.formData);
            switch (num) {
                case 1:
                    if ($scope.formData.grade1) {
                        if ($scope.formData.course1.length < 1) {
                            $scope.formData.grade1 = "";
                            $scope.getSummaryNumbers();
                        }
                        if ($scope.formData.course1.length > 2 && $scope.formData.grade1.length > 0) $scope.getSummaryNumbers();
                    }
                    break;
                case 2:
                    if ($scope.formData.grade2) {
                        if ($scope.formData.course2.length < 1) {
                            $scope.formData.grade2 = "";
                            $scope.getSummaryNumbers();
                        }
                        if ($scope.formData.course2.length > 2 && $scope.formData.grade2.length > 0) $scope.getSummaryNumbers();
                    }
                    break;
                case 3:
                    if ($scope.formData.grade3) {
                        if ($scope.formData.course3.length < 2) {
                            $scope.formData.grade3 = '';
                            $scope.getSummaryNumbers();
                        }
                        if ($scope.formData.course3.length > 2 && $scope.formData.grade3.length > 0) $scope.getSummaryNumbers();
                    }
                    break;
                case 4:
                    if ($scope.formData.grade4) {
                        if ($scope.formData.course4.length < 2) {
                            $scope.formData.grade4 = '';
                            $scope.getSummaryNumbers();
                        }
                        if ($scope.formData.course4.length > 2 && $scope.formData.grade4.length > 0) $scope.getSummaryNumbers();
                    }
                    break;
                case 5:
                    if ($scope.formData.grade5) {
                        if ($scope.formData.course5.length < 2) {
                            $scope.formData.grade5 = '';
                            $scope.getSummaryNumbers();
                        }
                        if ($scope.formData.course5.length > 2 && $scope.formData.grade5.length > 0) $scope.getSummaryNumbers();
                    }
                    break;
                case 6:
                    if ($scope.formData.grade6) {
                        if ($scope.formData.course6.length < 2) {
                            $scope.formData.grade6 = '';
                            $scope.getSummaryNumbers();
                        }
                        if ($scope.formData.course6.length > 2 && $scope.formData.grade6.length > 0) $scope.getSummaryNumbers();
                    }
                    break;
                case 7:
                    if ($scope.formData.grade7) {
                        if ($scope.formData.course7.length < 2) {
                            $scope.formData.grade7 = '';
                            $scope.getSummaryNumbers();
                        }
                        if ($scope.formData.course7.length > 2 && $scope.formData.grade7.length > 0) { $scope.getSummaryNumbers(); }
                    }
                    break;
                case 8:
                    if ($scope.formData.grade8) {
                        if ($scope.formData.course8.length < 2) {
                            $scope.formData.grade8 = '';
                            $scope.getSummaryNumbers();
                        }
                        if ($scope.formData.course8.length > 2 && $scope.formData.grade8.length > 0) $scope.getSummaryNumbers();
                    }
                    break;
            }
        }

        $scope.onSelect = function($item, $model, $label) {
            $scope.$item = $item;
            $scope.getSummaryNumbers();
        };

        $scope.checkEmpty = function() {
            //console.log("checkEmpty");
            if (!$scope.formData.grade1 && !$scope.formData.grade2 && !$scope.formData.grade3 && !$scope.formData.grade4 && !$scope.formData.grade5 && !$scope.formData.grade6 && !$scope.formData.grade7 && !$scope.formData.grade8) {
                $scope.resting = true;
                $scope.noresults_message = false;
                $scope.output.num_programs = -1;
                $scope.output.num_careers = -1;
                $scope.output.salary = -1;
            }
        }
        $scope.checkGrade = function(num) {

            console.log($scope.formData);

            switch (num) {
                case 1:
                    if ($scope.formData.grade1 > 100) $scope.formData.grade1 = 100;
                    $scope.getSummaryNumbers();
                    break;
                case 2:
                    if ($scope.formData.grade2 > 100) $scope.formData.grade2 = 100;
                    $scope.getSummaryNumbers();
                    break;
                case 3:
                    if ($scope.formData.grade3 > 100) $scope.formData.grade3 = 100;
                    $scope.getSummaryNumbers();
                    break;
                case 4:
                    if ($scope.formData.grade4 > 100) $scope.formData.grade4 = 100;
                    $scope.getSummaryNumbers();
                    break;
                case 5:
                    if ($scope.formData.grade5 > 100) $scope.formData.grade5 = 100;
                    $scope.getSummaryNumbers();
                    break;
                case 6:
                    if ($scope.formData.grade6 > 100) $scope.formData.grade6 = 100;
                    $scope.getSummaryNumbers();
                    break;
                case 7:
                    if ($scope.formData.grade7 > 100) $scope.formData.grade7 = 100;
                    $scope.getSummaryNumbers();
                    break;
                case 8:
                    if ($scope.formData.grade8 > 100) $scope.formData.grade8 = 100;
                    $scope.getSummaryNumbers();
                    break;
            }

        }

        // Function to get three summary numbers on inputting of courses and grades
        $scope.getSummaryNumbers = function() {

            $scope.loading = true;
            $scope.resting = false;

            $http({
                    method: 'POST',
                    url: '/api/get-summary-data/',
                    data: $.param($scope.formData), // pass in data as strings
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' } // set the headers so angular passing info as form data (not request payload)
                })
                .then(function(response) {
                    $scope.message = "success";
                    $scope.output = response.data;
                    $scope.loading = false;
                    $scope.resting = true;
                    if (response.data.num_programs == 0) {
                        $scope.noresults_message = true;
                    } else {
                        $scope.noresults_message = false;
                    }
                });

        }

        // on registration, send formData to backend to save in session
        $scope.saveSessionData = function() {
            console.log("called saveSessionData");
            var pageData = $scope.formData;
            pageData.summaryPrograms = $scope.output.num_programs;
            pageData.summaryCareers = $scope.output.num_careers;
            pageData.summarySalary = $scope.output.salary;

            $http({
                    method: 'POST',
                    url: '/api/save-session-data/',
                    data: $.param(pageData),
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' } // set the headers so angular passing info as form data (not request payload)
                })
                .then(function(response) {
                    $scope.message = "success";
                    console.log("user data saved");
                });
        }



        // send all formData to backend to save courses and grades in DB
        $scope.saveGrades = function() {
            console.log("called saveGrades");
            var pageData = $scope.formData;
            pageData.summaryPrograms = $scope.output.num_programs;
            pageData.summaryCareers = $scope.output.num_careers;
            pageData.summarySalary = $scope.output.salary;

            $http({
                    method: 'POST',
                    url: '/api/save-user-data/',
                    data: $.param(pageData),
                    //data    : params,
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' } // set the headers so angular passing info as form data (not request payload)
                })
                .success(function(data) {
                    $scope.message = "success";
                    //console.log(data);
                }).error(function() {
                    // handle errors
                    console.log("error occured while saving course and grade data");
                });
        }

        $scope.togglePersonality = function() {

            if ($scope.personalitySwitch) {
                //turning off
                $scope.personalitySwitch = false;
                $scope.getCareers('No');
            } else {
                // turning on
                $scope.personalitySwitch = true;
            }
        }

        // Fetches careers based on personality selection
      $scope.getCareers = function(personality) {
        //alert(personality);
          $scope.selectedCareers = [];
          $scope.tempArray = [];
          $scope.personality = personality;
          $scope.careerBtn = false;
          $scope.careersLoading = true;
          
          
          
          $("#filtertags").empty();
          $scope.cFiltersArray = [];
          $scope.demandFiltersArray = [];
          
          $scope.resetCareerLimit();
          $scope.activeFilter = false;
          
                //remove previous careers
         var elems = $grid.isotope('getItemElements')
          if(elems.length > 0 ){
            $grid.show( elems )
             /*for (var i = 0; i < elems.length;  i++) {
                 if ($( elems[i] ).hasClass('ng-hide'))  $(elems[i]).removeClass('ng-hide');
             }*/
             $grid.isotope( 'remove', elems ).isotope('layout');
                  }
              
          // $grid.isotope('destroy')
          // $grid = $('.grid').isotope($scope.IsotopeOptions);
           
           $grid.isotope({ sortBy: $scope.sortData });
          
          if(personality != 'No') {
            $scope.personalitySwitch = true;
          }
          
          dataLayer.push({
            'event': 'GTMevent',
            'eventCategory': 'personality-test', 
            'eventAction': 'personality-type-selection',
            'eventLabel': personality,
            'personalityType': personality,
            //'personalityCareer': '<personalityname>',
          });
          
          $http({
                method  : 'POST',
                url     : main_link + 'api/get-personality-careers/',
                data    :  $.param({'personality' : personality}),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
               })
              .success(function(data){
                console.log(data);

                setTimeout(function () {
                  $grid.isotope('reloadItems'); $grid.isotope('shuffle');  }, 100)
                  
                $("#selectCity").val("");
                $(".grid").addClass("careershiding");
              
                var size = 28;
                var url = window.location.href; 
                var title = "";
                 $scope.loggedin = data.loggedin;
				 console.log($scope.loggedin);
          
                var careers = JSON.parse(data.careers);
                careers.length > 28 ? size = 28 : size = careers.length;
                $scope.careers = careers;
          for (var i = 0; i < $scope.careers.length;  i++) {
                    if (url.indexOf("/fr/") >= 0)  title = $scope.careers[i].fields.name_french;  else  title = $scope.careers[i].fields.name;
            
                    if ($scope.careers[i].fields.toronto_low == 0) { $scope.careers[i].fields.toronto_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_toronto_salary = true;}
                    if ($scope.careers[i].fields.toronto_high == 0) { $scope.careers[i].fields.toronto_high = $scope.careers[i].fields.ontario_high}                     
                    if ($scope.careers[i].fields.ottawa_low == 0) { $scope.careers[i].fields.ottawa_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_ottawa_salary = true;}
                    if ($scope.careers[i].fields.ottawa_high == 0) { $scope.careers[i].fields.ottawa_high = $scope.careers[i].fields.ontario_high}                     
                    if ($scope.careers[i].fields.london_low == 0) { $scope.careers[i].fields.london_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_london_salary = true;}
                    if ($scope.careers[i].fields.london_high == 0) { $scope.careers[i].fields.london_high = $scope.careers[i].fields.ontario_high}                     
                    if ($scope.careers[i].fields.hamilton_niagra_low == 0) { $scope.careers[i].fields.hamilton_niagra_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_hamilton_niagra_salary = true;}
                    if ($scope.careers[i].fields.hamilton_niagra_high == 0) { $scope.careers[i].fields.hamilton_niagra_high = $scope.careers[i].fields.ontario_high}                     
                    if ($scope.careers[i].fields.kingston_pembrooke_low == 0) { $scope.careers[i].fields.kingston_pembrooke_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_kingston_pembrooke_salary = true;}
                    if ($scope.careers[i].fields.kingston_pembrooke_high == 0) { $scope.careers[i].fields.kingston_pembrooke_high = $scope.careers[i].fields.ontario_high}
                    if ($scope.careers[i].fields.kitchener_waterloo_barrie_low == 0) { $scope.careers[i].fields.kitchener_waterloo_barrie_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_kitchener_waterloo_barrie_salary = true;}
                    if ($scope.careers[i].fields.kitchener_waterloo_barrie_high == 0) { $scope.careers[i].fields.kitchener_waterloo_barrie_high = $scope.careers[i].fields.ontario_high}
                    if ($scope.careers[i].fields.muskoka_kawartha_low == 0) { $scope.careers[i].fields.muskoka_kawartha_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_muskoka_kawartha_salary = true;}
                    if ($scope.careers[i].fields.muskoka_kawartha_high == 0) { $scope.careers[i].fields.muskoka_kawartha_high = $scope.careers[i].fields.ontario_high}
                    if ($scope.careers[i].fields.northeast_low == 0) { $scope.careers[i].fields.northeast_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_northeast_salary = true;}
                    if ($scope.careers[i].fields.northeast_high == 0) { $scope.careers[i].fields.northeast_high = $scope.careers[i].fields.ontario_high}
                    if ($scope.careers[i].fields.northwest_low == 0) { $scope.careers[i].fields.northwest_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_northwest_salary = true;}
                    if ($scope.careers[i].fields.northwest_high == 0) { $scope.careers[i].fields.northwest_high = $scope.careers[i].fields.ontario_high}
                    if ($scope.careers[i].fields.stratford_bruce_peninsula_low == 0) { $scope.careers[i].fields.stratford_bruce_peninsula_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_stratford_bruce_peninsula_salary = true;}
                    if ($scope.careers[i].fields.stratford_bruce_peninsula_high == 0) { $scope.careers[i].fields.stratford_bruce_peninsula_high = $scope.careers[i].fields.ontario_high}     
                    if ($scope.careers[i].fields.windsor_sarnia_low == 0) { $scope.careers[i].fields.windsor_sarnia_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_windsor_sarnia_salary = true;}
                    if ($scope.careers[i].fields.windsor_sarnia_high == 0) { $scope.careers[i].fields.windsor_sarnia_high = $scope.careers[i].fields.ontario_high}  
                    
           $scope.$items = $( "<div  class='well well-sm element-item blurred' name='" + $scope.careers[i].fields.name + "' ng-click='getPrograms(careers[" + i + "].pk)' outlook='" + $scope.careers[i].fields.outlook_ontario + "' ottawa='" + $scope.careers[i].fields.outlook_ottawa +"'" + 
            "toronto_salary = '" + $scope.careers[i].fields.provincial_toronto_salary + "' ottawa_salary= '"+ $scope.careers[i].fields.provincial_ottawa_salary + "' london_salary = '"  + $scope.careers[i].fields.provincial_london_salary + "'" + 
            "hamilton_niagra_salary = '" +  $scope.careers[i].fields.provincial_hamilton_niagra_salary + "' kingston_pembrook_salary = '" + $scope.careers[i].fields.provincial_kingston_pembrooke_salary + "' kitchener_waterloo_barrie_salary = '" + $scope.careers[i].fields.provincial_kitchener_waterloo_barrie_salary + "'" + 
            "muskoka_kawartha_salary = '" + $scope.careers[i].fields.provincial_muskoka_kawartha_salary + "' northeast_salary = '" +  $scope.careers[i].fields.provincial_northeast_salary + "' northwest_salary = '" + $scope.careers[i].fields.provincial_northwest_salary +  "'" +
            "stratford_bruce_peninsula_salary = '" + $scope.careers[i].fields.provincial_stratford_bruce_peninsula_salary + "' windsor_sarnia_salary =  '" + $scope.careers[i].fields.provincial_windsor_sarnia_salary + "'" + 
            "toronto= '" + $scope.careers[i].fields.outlook_toronto + "' london='" +  $scope.careers[i].fields.outlook_london +"' hamilton_niagra='" + $scope.careers[i].fields.hamilton_niagra_outlook +"' kingston_pembrooke = '" + $scope.careers[i].fields.kingston_pembrooke_outlook  + "'" + 
            "kitchener_waterloo_barrie = '" +  $scope.careers[i].fields.kitchener_waterloo_barrie_outlook  + "' muskoka_kawartha = '" + $scope.careers[i].fields.muskoka_kawartha_outlook +"' northeast = '" + $scope.careers[i].fields.northeast_outlook + "' prov = '" + $scope.careers[i].fields.provincial_salary  + "'" +  
            "northwest = '" + $scope.careers[i].fields.northwest_outlook +"' stratford_bruce_peninsula = '" +  $scope.careers[i].fields.stratford_bruce_peninsula_outlook + "' toronto_high = '" + $scope.careers[i].fields.toronto_high + "'  toronto_low='" + $scope.careers[i].fields.toronto_low + "'"  + 
            "windsor_sarnia = '" + $scope.careers[i].fields.windsor_sarnia_outlook + "' data-toggle='popover' data-placement='top' data-content='' data-category='" + $scope.careers[i].fields.category + "'    data-outlook='" + $scope.careers[i].fields.outlook_ontario + "'" +  
            "ottawa_low='" + $scope.careers[i].fields.ottawa_low + "' ottawa_high='" + $scope.careers[i].fields.ottawa_high + "' london_low='" + $scope.careers[i].fields.london_low + "' london_high='" +  $scope.careers[i].fields.london_high + "'" +
            "hamilton_niagra_low = '" + $scope.careers[i].fields.hamilton_niagra_low + "' hamilton_niagra_high = '" + $scope.careers[i].fields.hamilton_niagra_high +"' kingston_pembrooke_low = '" + $scope.careers[i].fields.kingston_pembrooke_low  + "'  kingston_pembrooke_high = '" + $scope.careers[i].fields.kingston_pembrooke_high  +  "'" +
            "kitchener_waterloo_barrie_low  = '" + $scope.careers[i].fields.kitchener_waterloo_barrie_low  + "'   kitchener_waterloo_barrie_high = '" + $scope.careers[i].fields.kitchener_waterloo_barrie_high  + "' muskoka_kawartha_low = '" + $scope.careers[i].fields.muskoka_kawartha_low + "' muskoka_kawartha_high = '" + $scope.careers[i].fields.muskoka_kawartha_high + "'" +
            "northeast_low = '" + $scope.careers[i].fields.northeast_low  + "' northeast_high = '" + $scope.careers[i].fields.northeast_high  +"' northwest_low = '"  + $scope.careers[i].fields.northwest_low  + "' northwest_high = '" + $scope.careers[i].fields.northwest_high + "' stratford_bruce_peninsula_low = '" +  $scope.careers[i].fields.stratford_bruce_peninsula_low + "' stratford_bruce_peninsula_high = '" + $scope.careers[i].fields.stratford_bruce_peninsula_high + "'" +
            "windsor_sarnia_low = '" +  $scope.careers[i].fields.windsor_sarnia_low  + "' windsor_sarnia_high = '" + $scope.careers[i].fields.windsor_sarnia_high +"' > "+
            "<a href='https://www.monster.ca/types-of-careers' target='_blank' class='info-link' id='infobutton' data-toggle='popover' data-placement='top'  > <img onclick='$event.stopPropagation()' class='info-icon' src='/static/img/icon-info.svg' /> </a>" +
              "<img id='like' data-toggle='popover'  class='plus-icon' />" +
              "<h4 class='name'>" + title +"</h4>" +
              "<span unselectable='on' class='income' income='" +  $scope.careers[i].fields.income  + "'ontario_low='" + $scope.careers[i].fields.ontario_low + "' ontario_high='" + $scope.careers[i].fields.ontario_high + "'" + 
              " ng-show='" + $scope.careers[i].fields.ontario_low +" == null'> </span>" +
              "<div> <span  unselectable='on'  class='income' income='" + careers[i].fields.low + "'><span class='priceLow'></span>K - <span class='priceHigh'></span>K <span class='priceRound'></span></span> </div>" +
              "<br>" +
            "</div>");
            
              $scope.$items.attr('ng-class', "{ 'selected' : careers[" + i + " ].selected}");
              $scope.$items.find("#like").attr('ng-click',"careers[" + i + "].selected = !careers[" + i + "].selected; saveCareer(careers[" + i + "]); $event.stopPropagation()");
              $scope.$items.find("#like").attr('ng-src', "{$ careers[" + i + "].selected && '/static/img/like-hover.svg' || '/static/img/like.svg' $}");
              
             
             // if ( i > size && !($scope.$items.hasClass('ng-hide')) ) $scope.$items.addClass('ng-hide');
              if ( !data.loggedin  && i > 4 ) {  $scope.$items.find("span").remove();$scope.$items.find(".info-link").remove();$scope.$items.find("#like").remove(); $scope.$items.addClass('blurred'); }
              if ( !data.loggedin  && i < 4 ) {  $scope.$items.addClass('careershidingcards');$scope.$items.addClass('blurred'); }
              if ( data.loggedin) $scope.$items.addClass('careershidingcards');

               
      
            if (data.loggedin || (!data.loggedin &&  i < 4)){
              if ($scope.careers[i].fields.outlook_ontario == 1)
              $scope.$items.addClass('outlook1'); 
              else if ($scope.careers[i].fields.outlook_ontario == 2)
              $scope.$items.addClass('outlook2'); 
              else if ($scope.careers[i].fields.outlook_ontario == 3)
              $scope.$items.addClass('outlook3'); 
              else if ($scope.careers[i].fields.outlook_ontario == 0)
              $scope.$items.addClass('outlook0'); 
            }
              $compile($scope.$items)($scope) ;
              $grid.append($scope.$items).isotope( 'appended', $scope.$items);
                      
                }
               
                
                $scope.careersLoading = false;
                var elems = $grid.isotope('getItemElements')
                $grid.hide( elems )

                $timeout(function() {
                  //console.log('isotope init');
                  $scope.$broadcast('iso-init', {name:null, params:null});
                  }, 0)
              $(".hideLoad").show();
              }).error(function(){
                console.log("error occured while fetching careers");
              });
              // $scope.$apply();
              $scope.resetCareerLimit();
              
      }

        $scope.checkCategoryDisable = function(item) {
            return $scope.careers.find(career => career.fields.category == item)
        }

        $scope.checkDemandDisable = function(item) {
            return $scope.careers.find(career => career.fields.outlook_ontario == item)
        }

        $scope.careerFilterFunction = function() {
            return function(item) {
                if ($(".hidetemp").is(":visible"))
                    $(".careershidingcards").removeClass("blurred");
                //console.log(item)
                //  $("#isotopeContainer").isotope('reloadItems');
                if ($scope.cFiltersArray.length && $scope.demandFiltersArray.length)
                    return $scope.cFiltersArray.find(k => item.fields.category == k) && $scope.demandFiltersArray.find(j => item.fields.outlook_ontario == j)
                else if ($scope.cFiltersArray.length)
                    return $scope.cFiltersArray.find(k => item.fields.category == k);
                else if ($scope.demandFiltersArray.length)
                    return $scope.demandFiltersArray.find(j => item.fields.outlook_ontario == j)
                else
                    return item;
            }
        }

      $scope.addFilters = function (value) {
        $scope.activeFilter = true;
       var category  =  value
       var frenchcat = value
         var url = window.location.href; 
          if (url.indexOf("/fr/") >= 0) 
          {
            switch(frenchcat) {
              case "Administrative & Support" : frenchcat = "Administration & Support"; break;
              case "Arts & Design" : frenchcat = "Arts & Design"; break;
              case "Education" : frenchcat = "Éducation"; break;
              case "Finance & Economics" : frenchcat = "Finance et économie"; break;
              case "Health & Social Services" : frenchcat = "Santé et services sociaux"; break;
              case "Information Technology" : frenchcat = "Technologie de l'information"; break;
              case "Policy & Law" : frenchcat = "Politique et services juridiques"; break;
              case "Business Development & Sales" : frenchcat = "Ventes et développement d'affaires"; break;
              case "Social Science" : frenchcat = "Science Sociale"; break;
              case "Science & Engineering" :  frenchcat = "Science et Ingénierie"; break; 
              case "Supply & Project Management" : frenchcat = "Approvisionnement & gestion de projet"; break;
            }
          }
        // $scope.careerLimitAll = $scope.careerLimit 
      // $scope.careerLimit  = $scope.careers.length;
       //$scope.$apply(function(){$("#isotopeContainer").isotope('reloadItems');});
      // $grid.isotope('reloadItems');
       //alert($scope.careerLimit);
        if($scope.cFiltersArray.indexOf(category) == -1 && value) {
          $scope.cFiltersArray.push(category);
          
          var $ft = $( "<span style='margin:10px; display:inline-block; white-space: nowrap;'  class='filtertag' >" +
            "<div style='display:flex; vertical-align: bottom; text-align: bottom;'>"+
            "<div style= 'flex-basis:87%;'>" + frenchcat + " </div>" +
            "<button class='filtertagcancel' ng-click='testclickz()'   style='flex-basis:13%; margin-left:8px;'></button>" +
            "</div>" +
          "</span>" ).appendTo('#filtertags');
          
          filterTags($ft,category);
          }
          $('#categoryfilter').prop('selectedIndex',0);
         // $("#isotopeContainer").isotope('reloadItems');
         //$("#categoryfilter").val("");
         //$('#categoryfilter option[value=""]').prop('selected',true);
               // $scope.careerLimitAll = $scope.careerLimit 
        //  $grid.isotope('reloadItems');
       $grid.isotope({ sortBy: $scope.sortData });
       //$grid.isotope('reloadItems');
     
        $scope.$apply();
        
        //$scope.careerLimit  = $scope.careerLimitAll;
      }

    $scope.addDemandFilters = function (value) {
      $scope.activeFilter = true;
      var demand  =  $scope.careerdemandArray[value];
      var url = window.location.href; 
          if (url.indexOf("/fr/") >= 0) 
          {
            switch(demand) {
              case "No Data Available" : demand = "Données non disponible"; break;
              case "High Demand Career" : demand = "Carrière très demandée"; break;
              case "Average Demand Career" : demand = "Carrière moyennement demandée"; break;
              case "Low Demand Career" : demand = "Carrière peu demandée"; break;
          }
          }
        //var my_name= $.i18n('No Data Available')
          $scope.careerLimitAll = $scope.careerLimit 
          $scope.careerLimit  = $scope.careers.length;
        var tempfilterNum  = value 
        if ( value  == 0 ) tempfilterNum = 4;
        
        if($scope.demandFiltersArray.indexOf(value) == -1 && value) {
          $scope.demandFiltersArray.push(value);
         var $ft = $("<span style='margin:10px; display:inline-block; white-space: nowrap;' class='filtertag'>" +
            "<div style='display:flex; vertical-align: bottom; text-align: bottom;'>" +
            "<div style= 'flex-basis:10%;'><span id='circle" + tempfilterNum  +"' class='circle'></span></div>" + 
            "<div style= 'flex-basis:77%;'>" + demand + "</div>" +
            "<button class='filtertagcancel' ng-click=removeFilter(" + value + ")'  style='flex-basis:13%; margin-left:8px;'></button>" +
            "</div>" +
          "</span>").appendTo('#filtertags');
         
          filterdemandTags($ft,value);
          }
         $('#demandfilter').prop('selectedIndex',0);
       // $scope.resetCareerLimit();
       $grid.isotope({ sortBy: $scope.sortData });
        $scope.$apply();
        
        
      }


      $scope.removeFilter = function(category){
         var index = $scope.cFiltersArray.indexOf(category);
         $scope.cFiltersArray.splice(index, 1);
         
        if ($scope.cFiltersArray.length < 1 && $scope.demandFiltersArray.length < 1){
           $scope.resetCareerLimit();
           $scope.activeFilter = false;
           $scope.$apply();
        }
        $grid.isotope({ sortBy: $scope.sortData });
      }
      
     
      
      
      $scope.removedemandFilter = function(category){
         var index = $scope.demandFiltersArray.indexOf(category);
         $scope.demandFiltersArray.splice(index, 1);
         if ($scope.cFiltersArray.length < 1 && $scope.demandFiltersArray.length < 1) {
          $scope.resetCareerLimit();
          $scope.activeFilter = false;
          $scope.$apply();
         }
          $grid.isotope({ sortBy: $scope.sortData });
      }
      
      
      $scope.resetCareerLimit = function() {
        if( navigator.userAgent.match(/Android/i)
         || navigator.userAgent.match(/webOS/i)
         || navigator.userAgent.match(/iPhone/i)
         || navigator.userAgent.match(/iPad/i)
         || navigator.userAgent.match(/iPod/i)
         || navigator.userAgent.match(/BlackBerry/i)
         || navigator.userAgent.match(/Windows Phone/i)
         ){
           
            $scope.careerLimit = 4;
          }
          else {
            
            $scope.careerLimit = 28;
            //alert($scope.careerLimit + "  " +  $scope.careers.length);
          }

      }
      
      $scope.loadMore = function() {
        $scope.tempArray=[];
         if(navigator.userAgent.match(/Android/i)
         || navigator.userAgent.match(/webOS/i)
         || navigator.userAgent.match(/iPhone/i)
         || navigator.userAgent.match(/iPad/i)
         || navigator.userAgent.match(/iPod/i)
         || navigator.userAgent.match(/BlackBerry/i)
         || navigator.userAgent.match(/Windows Phone/i)
         ){
         
              var incremented = $scope.careerLimit + 4;
          }
          else {
            /*count  = 0
            var elems = $grid.isotope('getItemElements')
            for (var i = 0; i < elems.length;  i++) {
            // $( elems[i] ).find('.weight').text();
             //console.log(html[0]);
              if ($( elems[i] ).hasClass('ng-hide')  && count < 28 ) {count++; $( elems[i] ).removeClass('ng-hide');}
            }*/
            // $grid.isotope({ sortBy: $scope.sortData });
           // $grid.isotope('reloadItems');
            //alert("lol");
            //$scope.categoryfilter = "";
            var incremented = $scope.careerLimit + 28;
       // $scope.$apply();
       // $("#isotopeContainer").isotope('reloadItems');
          }
        $scope.careerLimit = incremented > $scope.careers.length ? $scope.careers.length : incremented;
             var elems = $grid.isotope('getItemElements')
             
             for ( var j = 0; j < $scope.careerLimit ; j++)
             {
             $scope.tempArray.push ($scope.careers[j].fields.name)
             }
            
            // elems.show();
          //   $grid.isotope( 'reveal', elems )
        //alert($scope.careerLimit + "  " +  $scope.careers.length);
       // $grid.isotope({ sortBy: $scope.sortData });
        $grid.isotope({ sortBy: $scope.sortData });
      };
      
      $scope.updateResults = function(personality) {
          $scope.selectedCareers = [];
          $scope.personality = personality;
          $scope.careerBtn = false;
          $scope.careersLoading = true;
          var pageData = $scope.formData;
          pageData.personality = personality
          
          $("#filtertags").empty();
          $scope.cFiltersArray = [];
          $scope.tempArray = [];
          $scope.demandFiltersArray = [];
          
          $scope.resetCareerLimit();
          $scope.activeFilter = false;
          
                //remove previous careers
          var elems = $grid.isotope('getItemElements')
          if(elems.length > 0 ){
            $grid.show( elems )
             /*for (var i = 0; i < elems.length;  i++) {
                 if ($( elems[i] ).hasClass('ng-hide')  )  $( elems[i] ).removeClass('ng-hide');
             }*/
             
             $grid.isotope( 'remove', elems ).isotope('layout');
                       
                  }
                
          $http({
                method  : 'POST',
                url     : main_link + 'api/update-results/',
                data    : $.param(pageData),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
               })
              .success(function(data){
                setTimeout(function () {
                  $grid.isotope('reloadItems'); $grid.isotope('shuffle');  }, 500)
                $scope.loggedin = data.loggedin;
                $("#selectCity").val("");
                $(".grid").addClass("careershiding");
              
                //var size = 28;
                var url = window.location.href; 
                var title = "";
                
          
                var careers = JSON.parse(data.careers);
                //careers.length > 28 ? size = 28 : size = careers.length;
                $scope.careers = careers;
               // $scope.careers = $scope.$eval(data.careers);
                
              for (var i = 0; i < $scope.careers.length;  i++) {
                  if (url.indexOf("/fr/") >= 0)  title =  $scope.careers[i].fields.name_french;  else  title= $scope.careers[i].fields.name;
                    if ($scope.careers[i].fields.toronto_low == 0) { $scope.careers[i].fields.toronto_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_toronto_salary = true;}
                    if ($scope.careers[i].fields.toronto_high == 0) { $scope.careers[i].fields.toronto_high = $scope.careers[i].fields.ontario_high}                     
                    if ($scope.careers[i].fields.ottawa_low == 0) { $scope.careers[i].fields.ottawa_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_ottawa_salary = true;}
                    if ($scope.careers[i].fields.ottawa_high == 0) { $scope.careers[i].fields.ottawa_high = $scope.careers[i].fields.ontario_high}                     
                    if ($scope.careers[i].fields.london_low == 0) { $scope.careers[i].fields.london_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_london_salary = true;}
                    if ($scope.careers[i].fields.london_high == 0) { $scope.careers[i].fields.london_high = $scope.careers[i].fields.ontario_high}                     
                    if ($scope.careers[i].fields.hamilton_niagra_low == 0) { $scope.careers[i].fields.hamilton_niagra_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_hamilton_niagra_salary = true;}
                    if ($scope.careers[i].fields.hamilton_niagra_high == 0) { $scope.careers[i].fields.hamilton_niagra_high = $scope.careers[i].fields.ontario_high}                     
                    if ($scope.careers[i].fields.kingston_pembrooke_low == 0) { $scope.careers[i].fields.kingston_pembrooke_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_kingston_pembrooke_salary = true;}
                    if ($scope.careers[i].fields.kingston_pembrooke_high == 0) { $scope.careers[i].fields.kingston_pembrooke_high = $scope.careers[i].fields.ontario_high}
                    if ($scope.careers[i].fields.kitchener_waterloo_barrie_low == 0) { $scope.careers[i].fields.kitchener_waterloo_barrie_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_kitchener_waterloo_barrie_salary = true;}
                    if ($scope.careers[i].fields.kitchener_waterloo_barrie_high == 0) { $scope.careers[i].fields.kitchener_waterloo_barrie_high = $scope.careers[i].fields.ontario_high}
                    if ($scope.careers[i].fields.muskoka_kawartha_low == 0) { $scope.careers[i].fields.muskoka_kawartha_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_muskoka_kawartha_salary = true;}
                    if ($scope.careers[i].fields.muskoka_kawartha_high == 0) { $scope.careers[i].fields.muskoka_kawartha_high = $scope.careers[i].fields.ontario_high}
                    if ($scope.careers[i].fields.northeast_low == 0) { $scope.careers[i].fields.northeast_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_northeast_salary = true;}
                    if ($scope.careers[i].fields.northeast_high == 0) { $scope.careers[i].fields.northeast_high = $scope.careers[i].fields.ontario_high}
                    if ($scope.careers[i].fields.northwest_low == 0) { $scope.careers[i].fields.northwest_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_northwest_salary = true;}
                    if ($scope.careers[i].fields.northwest_high == 0) { $scope.careers[i].fields.northwest_high = $scope.careers[i].fields.ontario_high}
                    if ($scope.careers[i].fields.stratford_bruce_peninsula_low == 0) { $scope.careers[i].fields.stratford_bruce_peninsula_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_stratford_bruce_peninsula_salary = true;}
                    if ($scope.careers[i].fields.stratford_bruce_peninsula_high == 0) { $scope.careers[i].fields.stratford_bruce_peninsula_high = $scope.careers[i].fields.ontario_high}     
                    if ($scope.careers[i].fields.windsor_sarnia_low == 0) { $scope.careers[i].fields.windsor_sarnia_low = $scope.careers[i].fields.ontario_low; $scope.careers[i].fields.provincial_windsor_sarnia_salary = true;}
                    if ($scope.careers[i].fields.windsor_sarnia_high == 0) { $scope.careers[i].fields.windsor_sarnia_high = $scope.careers[i].fields.ontario_high}
                    
            $scope.$items = $( "<div  class='well well-sm element-item blurred'  name='" + $scope.careers[i].fields.name + "'  ng-click='getPrograms(careers[" + i + "].pk)' outlook='" + $scope.careers[i].fields.outlook_ontario + "' ottawa='" + $scope.careers[i].fields.outlook_ottawa +"'" + 
            "toronto_salary = '" + $scope.careers[i].fields.provincial_toronto_salary + "' ottawa_salary= '"+ $scope.careers[i].fields.provincial_ottawa_salary + "' london_salary = '"  + $scope.careers[i].fields.provincial_london_salary + "'" + 
            "hamilton_niagra_salary = '" +  $scope.careers[i].fields.provincial_hamilton_niagra_salary + "' kingston_pembrook_salary = '" + $scope.careers[i].fields.provincial_kingston_pembrooke_salary + "' kitchener_waterloo_barrie_salary = '" + $scope.careers[i].fields.provincial_kitchener_waterloo_barrie_salary + "'" + 
            "muskoka_kawartha_salary = '" + $scope.careers[i].fields.provincial_muskoka_kawartha_salary + "' northeast_salary = '" +  $scope.careers[i].fields.provincial_northeast_salary + "' northwest_salary = '" + $scope.careers[i].fields.provincial_northwest_salary +  "'" +
            "stratford_bruce_peninsula_salary = '" + $scope.careers[i].fields.provincial_stratford_bruce_peninsula_salary + "' windsor_sarnia_salary =  '" + $scope.careers[i].fields.provincial_windsor_sarnia_salary + "'" + 
            "toronto= '" + $scope.careers[i].fields.outlook_toronto + "' london='" +  $scope.careers[i].fields.outlook_london +"' hamilton_niagra='" + $scope.careers[i].fields.hamilton_niagra_outlook +"' kingston_pembrooke = '" + $scope.careers[i].fields.kingston_pembrooke_outlook  + "'" + 
            "kitchener_waterloo_barrie = '" +  $scope.careers[i].fields.kitchener_waterloo_barrie_outlook  + "' muskoka_kawartha = '" + $scope.careers[i].fields.muskoka_kawartha_outlook +"' northeast = '" + $scope.careers[i].fields.northeast_outlook + "' prov = '" + $scope.careers[i].fields.provincial_salary  + "'" +  
            "northwest = '" + $scope.careers[i].fields.northwest_outlook +"' stratford_bruce_peninsula = '" +  $scope.careers[i].fields.stratford_bruce_peninsula_outlook + "' toronto_high = '" + $scope.careers[i].fields.toronto_high + "'  toronto_low='" + $scope.careers[i].fields.toronto_low + "'"  + 
            "windsor_sarnia = '" + $scope.careers[i].fields.windsor_sarnia_outlook + "' data-toggle='popover' data-placement='top' data-content='' data-category='" + $scope.careers[i].fields.category + "'    data-outlook='" + $scope.careers[i].fields.outlook_ontario + "'" +  
            "ottawa_low='" + $scope.careers[i].fields.ottawa_low + "' ottawa_high='" + $scope.careers[i].fields.ottawa_high + "' london_low='" + $scope.careers[i].fields.london_low + "' london_high='" +  $scope.careers[i].fields.london_high + "'" +
            " hamilton_niagra_low = '" + $scope.careers[i].fields.hamilton_niagra_low + "' hamilton_niagra_high = '" + $scope.careers[i].fields.hamilton_niagra_high +"' kingston_pembrooke_low = '" + $scope.careers[i].fields.kingston_pembrooke_low  + "'  kingston_pembrooke_high = '" + $scope.careers[i].fields.kingston_pembrooke_high  +  "'" +
            "kitchener_waterloo_barrie_low  = '" + $scope.careers[i].fields.kitchener_waterloo_barrie_low  + "'   kitchener_waterloo_barrie_high = '" + $scope.careers[i].fields.kitchener_waterloo_barrie_high  + "' muskoka_kawartha_low = '" + $scope.careers[i].fields.muskoka_kawartha_low + "' muskoka_kawartha_high = '" + $scope.careers[i].fields.muskoka_kawartha_high + "'" +
            "northeast_low = '" + $scope.careers[i].fields.northeast_low  + "' northeast_high = '" + $scope.careers[i].fields.northeast_high  +"' northwest_low = '"  + $scope.careers[i].fields.northwest_low  + "' northwest_high = '" + $scope.careers[i].fields.northwest_high + "' stratford_bruce_peninsula_low = '" +  $scope.careers[i].fields.stratford_bruce_peninsula_low + "' stratford_bruce_peninsula_high = '" + $scope.careers[i].fields.stratford_bruce_peninsula_high + "'" +
            "windsor_sarnia_low = '" +  $scope.careers[i].fields.windsor_sarnia_low  + "' windsor_sarnia_high = '" + $scope.careers[i].fields.windsor_sarnia_high +"' > "+
            "<a href='https://www.monster.ca/types-of-careers' target='_blank' class='info-link' id='infobutton' data-toggle='popover' data-placement='top'  > <img onclick='$event.stopPropagation()' class='info-icon' src='/static/img/icon-info.svg' /> </a>" +
              "<img id='like' data-toggle='popover'  class='plus-icon' />" +
              "<h4 class='name'>" + title +"</h4>" +
              "<span unselectable='on' class='income' income='" +  $scope.careers[i].fields.income  + "'ontario_low='" + $scope.careers[i].fields.ontario_low + "' ontario_high='" + $scope.careers[i].fields.ontario_high + "'" + 
              " ng-show='" + $scope.careers[i].fields.ontario_low +" == null'> </span>" +
              "<div> <span  unselectable='on'  class='income' income='" + careers[i].fields.low + "'><span class='priceLow'></span>K - <span class='priceHigh'></span>K <span class='priceRound'></span></span> </div>" +
              "<br>" +
            "</div>");
            
              $scope.$items.attr('ng-class', "{ 'selected' : careers[" + i + "].selected}");
              $scope.$items.find("#like").attr('ng-click'," careers[" + i + "].selected = !careers[" + i + "].selected; saveCareer(careers[" + i + "]); $event.stopPropagation()");
              $scope.$items.find("#like").attr('ng-src', "{$ careers[" + i + "].selected && '/static/img/like-hover.svg' || '/static/img/like.svg' $}");
              
              
              //if ( i > size ) $scope.$items.addClass('ng-hide');
              if ( !data.loggedin  && i > 4 ) {  $scope.$items.find("span").remove();$scope.$items.find(".info-link").remove();$scope.$items.find("#like").remove(); $scope.$items.addClass('blurred'); }
              if ( !data.loggedin  && i < 4 ) {  $scope.$items.addClass('careershidingcards');$scope.$items.addClass('blurred'); }
              if ( data.loggedin) $scope.$items.addClass('careershidingcards'); 
      
      
            if (data.loggedin || (!data.loggedin &&  i < 4)){
              if ($scope.careers[i].fields.outlook_ontario == 1)
              $scope.$items.addClass('outlook1'); 
              else if ($scope.careers[i].fields.outlook_ontario == 2)
              $scope.$items.addClass('outlook2'); 
              else if ($scope.careers[i].fields.outlook_ontario == 3)
              $scope.$items.addClass('outlook3'); 
              else if ($scope.careers[i].fields.outlook_ontario == 0)
              $scope.$items.addClass('outlook0'); 
            }
              $compile($scope.$items)($scope) ;
              $grid.append($scope.$items ).isotope( 'appended', $scope.$items);
              
                }
                $scope.careersLoading = false;
                console.log(Object.keys(data.careers).length);
                //console.log(data);
                $timeout(function() {
                  //console.log('isotope init');
                  $scope.$broadcast('iso-init', {name:null, params:null});
                  }, 1000 )
                
              }).error(function(){
                console.log("error occured while fetching careers");
              });
      }


        $scope.saveCareer = function(career) {

            if (career.selected) {
                $http({
                        method: 'POST',
                        url: '/api/add-career/',
                        data: $.param({ 'career': career.pk }),
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' } // set the headers so angular passing info as form data (not request payload)
                    })
                    .success(function(data) {
                        console.log(data);
                    }).error(function() {
                        console.log("error occured while fetching careers");
                    });
            } else {
                $http({
                        method: 'POST',
                        url: '/api/remove-career/',
                        data: $.param({ 'career': career.pk }),
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' } // set the headers so angular passing info as form data (not request payload)
                    })
                    .success(function(data) {
                        console.log(data);
                    }).error(function() {
                        console.log("error occured while fetching careers");
                    });
            }

        }


        $scope.removeCareer = function(career) {

            $http({
                    method: 'POST',
                    url: '/api/remove-career/',
                    data: $.param({ 'career': career }),
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' } // set the headers so angular passing info as form data (not request payload)
                })
                .success(function(data) {
                    $scope.careers = $scope.$eval(data.careers);
                    console.log(data);
                }).error(function() {
                    console.log("error occured while fetching careers");
                });


        }

        // Fetches programs based on career selections
        $scope.getPrograms = function(careerId) {
            $scope.sortType = ''; // set the default sort type
            $scope.sortReverse = false; // set the default sort order
            $scope.programsLoading = true;
            var jsonData = angular.toJson($scope.selectedCareers);
            //alert(jsonData);

            $http({
                    method: 'POST',
                    url: '/api/get-programs/',
                    //data    :  $.param({codeslist: jsonData}), // this one worked for sending list of career ids
                    data: $.param({ 'careerid': careerId }),
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' } // set the headers so angular passing info as form data (not request payload)
                })
                .then(function(response) {
                    $scope.programs = response.data.programs;
                    $scope.scholarships = response.data.scholarships;
                    $scope.numPrograms = response.data.num_programs;
                    $scope.programsLoading = false;
                });
        }

        // Gets user careers to dashboard
        $scope.getUserCareers = function() {

            $http.get('/api/get-user-careers/').then(function(response) {
                $scope.careers = response.data;
                console.log(response.data);
            });
        }

        /*
        $scope.sendFeedback = function() {
          
            $http({
                  method  : 'POST',
                  url     : main_link + 'api/send-feedback/',
                  data    : $.param($scope.feedback),  // pass in data as strings
                  headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
                 })
                .success(function(data){
                  console.log(data);
                }).error(function(){
                  console.log("error occured while sending feedback");
                }); 
        }
        */
    }
]);