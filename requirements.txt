Django==2.0
django-angular==2.2
django-nocaptcha-recaptcha==0.0.20
django-xforwardedfor-middleware==2.0
djangorestframework==3.9.3
geoip2==2.9.0
maxminddb==1.4.1
mysqlclient==1.4.2.post1
pytz==2019.1
requests==2.21.0
sqlparse==0.3.0
django-paypal