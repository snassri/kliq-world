from rest_framework import serializers
from journey.models import Course, Grades, Program, Career

class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ('id', 'name_english', 'name_french')
        
        
class GradesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Grades
        fields = '__all__' 

class CareerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Career
        fields = '__all__' 
        
class ProgramSerializer(serializers.ModelSerializer):
    class Meta:
        model = Program
        fields = '__all__' 

class Summary(object):
    def __init__(self, programs, careers, salary, course1, course2, course3, course4, course5, course6, course7, course8, grade1, grade2, grade3, grade4, grade5, grade6, grade7, grade8):
        self.programs = programs
        self.careers = careers
        self.salary = salary
        self.course1 = course1
        self.course2 = course2
        self.course3 = course3
        self.course4 = course4
        self.course5 = course5
        self.course6 = course6
        self.course7 = course7
        self.course8 = course8
        self.grade1 = grade1
        self.grade2 = grade2
        self.grade3 = grade3
        self.grade4 = grade4
        self.grade5 = grade5
        self.grade6 = grade6
        self.grade7 = grade7
        self.grade8 = grade8
        
        
class SummarySerializer(serializers.Serializer):
    programs = serializers.IntegerField()
    careers = serializers.IntegerField()
    salary = serializers.IntegerField()
    course1 = serializers.CharField(max_length=50, allow_null=True)
    course2 = serializers.CharField(max_length=50, allow_null=True)
    course3 = serializers.CharField(max_length=50, allow_null=True)
    course4 = serializers.CharField(max_length=50, allow_null=True)
    course5 = serializers.CharField(max_length=50, allow_null=True)
    course6 = serializers.CharField(max_length=50, allow_null=True)
    course7 = serializers.CharField(max_length=50, allow_null=True)
    course8 = serializers.CharField(max_length=50, allow_null=True)
    grade1 = serializers.IntegerField(allow_null=True)
    grade2 = serializers.IntegerField(allow_null=True)
    grade3 = serializers.IntegerField(allow_null=True)
    grade4 = serializers.IntegerField(allow_null=True)
    grade5 = serializers.IntegerField(allow_null=True)
    grade6 = serializers.IntegerField(allow_null=True)
    grade7 = serializers.IntegerField(allow_null=True)
    grade8 = serializers.IntegerField(allow_null=True)
    
    
    def create(self, validated_data):
        return Summary(**validated_data)
