from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm, PasswordResetForm
from journey.models import Profile, Country_Diploma
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from nocaptcha_recaptcha.fields import NoReCaptchaField

USER_TYPE = (
    ('', '---'),
    ("hs student", _("High school student")),
    ("uni student", _("University student")),
    ("parent", _("Parent")),
    ("counselor", _("Counselor")),
    ("other", _("Other")),
)

GENDER = (
    ('', '---'),
    ('female', _('Female')), 
    ('male', _('Male')), 
    ('nosay', _('Prefer not to say'))
)

SCHOOL_LANGUAGE = (
    ('english', _('Yes, English')), 
    ('french', _('Yes, French')), 
    ('no', _("No, I haven't"))
)

GRADES = (
    ('', '---'),
    (9, 9),
    (10, 10),
    (11, 11),
    (12, 12),
    (1, _('1st year')),
    (2, _('2nd year')),
    (3, _('3rd year')),
    (4, _('4th year')),
)

class RegistrationForm(UserCreationForm):
    
    name = forms.CharField(widget=forms.TextInput(attrs={'autocomplete': 'off', 'name':'name','spellcheck':'false'}))
    # email = forms.EmailField(required=True, widget=forms.EmailInput(attrs={'data-gtmtag': 'Email'}))
    email2 = forms.EmailField(required=True, widget=forms.EmailInput())
    password1 = forms.CharField(required=True, widget=forms.TextInput(attrs={'type':'password', 'autocomplete': 'off','spellcheck':'false'}))
    password2 = forms.CharField(required=True, widget=forms.TextInput(attrs={'type':'password', 'autocomplete': 'off','spellcheck':'false'}))
    school_language = forms.ChoiceField(widget=forms.RadioSelect, choices=SCHOOL_LANGUAGE)
    user_type = forms.ChoiceField(required=True, choices=USER_TYPE, widget=forms.Select(attrs={'data-gtmtag':'Are you a?'}))
    gender = forms.ChoiceField(required=True, choices=GENDER, widget=forms.Select(attrs={'data-gtmtag':'Gender'}))
    date_birth = forms.DateField(required=False, input_formats=['%m/%Y'], widget=forms.DateInput(attrs={'data-gtmtag':'Birth Month & Year'}))
    promo_code = forms.CharField(required=False, widget=forms.TextInput(attrs={'autocomplete': 'off', 'name':'promo_code','spellcheck':'false',}))
    # grade = forms.ChoiceField(required=False, choices=GRADES, widget=forms.Select(attrs={'data-gtmtag':'Grade'}))
    captcha = NoReCaptchaField()
    
    class Meta:
        model = Profile
        fields = ("name", "email", "email2", "password1", "password2", "user_type", "gender", "mother_tongue", "date_birth", "school_language", "grade", "city", "captcha", "promo_code")
    
    
    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['email'].widget.attrs['class'] = 'form-control'
        self.fields['email2'].widget.attrs['class'] = 'form-control'
        self.fields['password1'].widget.attrs['class'] = 'form-control'
        self.fields['password2'].widget.attrs['class'] = 'form-control'
        self.fields['grade'].widget.attrs['class'] = 'form-control'
        self.fields['user_type'].widget.attrs['class'] = 'form-control'
        self.fields['mother_tongue'].widget.attrs['class'] = 'form-control'
        self.fields['gender'].widget.attrs['class'] = 'form-control'
        self.fields['date_birth'].widget.attrs['class'] = 'form-control'
        self.fields['city'].widget.attrs['class'] = 'form-control'
        self.fields['promo_code'].widget.attrs['class'] = 'form-control'
        
        if self.instance.pk:
            self.fields['firstname'].required = True
            self.fields['lastname'].required = True
            self.fields['password1'].required = True
            self.fields['password2'].required = True 
    
    def clean_email(self):
        email = self.cleaned_data['email']
        if Profile.objects.filter(email__iexact=email).exists():
            raise ValidationError("This email has already been used!")
        return email

    
class CountryForm(forms.ModelForm):
    class Meta:
        model = Country_Diploma
        fields = ("country_name", "school_diploma")
        
        labels = {
            'name': 'Country'
        }      
 
class LoginForm(AuthenticationForm):
    
    username = forms.EmailField(required=True, widget=forms.TextInput(attrs={'placeholder':_('Email address'),}), error_messages= {"required": "Give yourself a username"})
    password = forms.CharField(required=True, widget=forms.TextInput(attrs={'type':'password', 'placeholder':_('Password'), }))
    
    
    class Meta:
        model = Profile
        fields = ("username", "password")
        error_messages= {
            "username" : {
        "required": "Give yourself a username"
        }
            
        }
        
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['class'] = 'form-control'
        self.fields['password'].widget.attrs['class'] = 'form-control'
        
        self.fields['username'].widget.attrs['error_messages'] = {'required': 'Please enter your name'}
        self.fields['password'].widget.attrs['error_messages'] = {'required': 'Please enter your password'}        
     
        
        
class MyPasswordResetForm(PasswordResetForm):
    
    email = forms.EmailField(required=True, widget=forms.TextInput(attrs={'placeholder':_('Email address'), 'type':'email'}))
    
    def __init__(self, *args, **kwargs):
        super(MyPasswordResetForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs['class'] = 'form-control'
        
    def clean_email(self):
        email = self.cleaned_data['email']
        if not Profile.objects.filter(email__iexact=email).exists():
            raise ValidationError(_("There is no user registered with the specified email address!"))
        
        return email