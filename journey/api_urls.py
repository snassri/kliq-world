from django.conf.urls import url
from journey import apiviews as api_views
from rest_framework import generics
from journey.serializers import CourseSerializer
from journey.models import Course

urlpatterns = [
    url(r'^get-summary-data/', api_views.get_summary_data, name='get-summary-data'),
    url(r'^save-session-data/', api_views.save_session_data, name='save-session-data'),
    url(r'^save-user-data/', api_views.save_user_data, name='save-user-data'),
    #url(r'^api/get-all-courses/$', api_views.get_all_courses, name='get_all_courses'),
    url(r'^get-courses/', generics.ListAPIView.as_view(queryset=Course.objects.all(), serializer_class=CourseSerializer), name='get-courses'),
    #url(r'^get-user-data/$', api_views.get_user_data, name='get-user-data'),
    url(r'^get-user-data/', api_views.UserData.as_view(), name='get-user-data'),
    url(r'^get-session-data/', api_views.GetSessionData.as_view(), name='get-session-data'),
    url(r'^get-personality-careers/', api_views.get_personality_careers, name='get-personality-careers'),
    url(r'^update-results/$', api_views.update_results, name='update-results'),
    url(r'^add-career/$', api_views.add_career, name='add-career'),
    url(r'^remove-career/$', api_views.remove_career, name='remove-career'),
    url(r'^get-user-careers/', api_views.get_user_careers, name='get-user-careers'),
    url(r'^get-programs/$', api_views.get_programs, name='get-program'),
    url(r'^send-feedback/$', api_views.send_feedback, name='send-feedback'),
    url(r'^check-email-exists/$', api_views.check_email_exists, name='check-email-exists'),
    url(r'^get-diploma/$', api_views.get_diploma_from_country, name='get-diploma'),
    url(r'^get-user-country/$', api_views.get_user_country, name='get-user-country'), 
]