from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from django.utils.translation import ugettext_lazy as _
import uuid

RELATION_TYPE = (
    ('Alone', 'Alone'),
    ('One of', 'One of'),
    ('Two of', 'Two of'),
)
RELATION_TYPE_2 = (
    (None, '--'),
    ('One of', 'One of'),
    ('Two of', 'Two of'),
)

COURSE_GROUP = (
    ('Arts', 'Arts'),
    ('Languages', 'Languages'),
    ('Humanities', 'Humanities'),
    ('Math', 'Math'),
    ('Science', 'Science'),
    ('Technology', 'Technology'),
)
PROVINCES = (
    ('Alberta', 'Alberta'),
    ('British Columbia', 'British Columbia'),
    ('Manitoba', 'Manitoba'),
    ('New Brunswick', 'New Brunswick'),
    ('Newfoundland and Labrador', 'Newfoundland and Labrador'),
    ('Nova Scotia', 'Nova Scotia'),
    ('Northwest Territories', 'Northwest Territories'),
    ('Nunavut', 'Nunavut'),
    ('Ontario', 'Ontario'),
    ('Prince Edward Island', 'Prince Edward Island'),
    ('Quebec', 'Quebec'),
    ('Saskatchewan', 'Saskatchewan'),
    ('Yukon', 'Yukon'),
)

LANG = (
    ("en", "English"),
    ("fr", "French"),
)

# USER_TYPE = (
#     ("student", "Student"),
#     ("parent", "Parent"),
#     ("counselor", "Counselor"),
#     ("other", "Other"),
# )

# GENDER = (
#     ('female', 'Female'), 
#     ('male', 'Male',), 
#     ('nosay', 'Prefer not to say')
# )

SCHOOL_LANGUAGE = (
    ('english', 'Yes, English'), 
    ('french', 'Yes, French'), 
    ('no', "No, I haven't")
)

class MyUserManager(BaseUserManager):
    def create_user(self, email, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')
            
        user = self.model(
            email=MyUserManager.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email and password.
        """
        u = self.create_user(
                        email=email,
                        password=password,
                    )
        u.is_admin = True
        u.is_staff = True
        u.is_active = True
        u.save(using=self._db)
        return u

class Personality(models.Model):
    code = models.CharField(max_length=6, blank=False)
    name = models.CharField(max_length=50, blank=True)
    
# domain shared by careers and programs, used as the link between them
class Domain(models.Model):
    name = models.CharField(max_length=255)  
    
    def __unicode__(self):
        string = self.name
        return string

class Career_Domain(models.Model):
    career = models.ForeignKey('Career', on_delete=models.CASCADE)
    domain = models.ForeignKey('Domain', on_delete=models.CASCADE, null=True)
    level = models.IntegerField( choices=[(1,1), (2,2), (3,3)] )
   
class Career(models.Model):
    name = models.CharField(max_length=255)
    name_french = models.CharField(max_length=255, default=None)
    jobbank_code = models.IntegerField(null=True)
    income = models.FloatField(blank=True)
    degree = models.CharField(max_length=255, blank=True)
    personality = models.ManyToManyField(Personality)
    category = models.CharField(max_length=50, null=True)
    domain = models.ManyToManyField(Domain, through=Career_Domain)
    ontario_low = models.IntegerField(null=True)
    ontario_high = models.IntegerField(null=True)
    canada_low = models.IntegerField(null=True)
    canada_high = models.IntegerField(null=True)
    ottawa_low = models.IntegerField(null=True)
    toronto_low = models.IntegerField(null=True)
    london_low = models.IntegerField(null=True)
    ottawa_high = models.IntegerField(null=True)
    toronto_high = models.IntegerField(null=True)
    london_high = models.IntegerField(null=True)
    link_jobbank_salary = models.CharField(max_length=255, default=None)
    outlook_ontario = models.IntegerField(null=True)
    outlook_ottawa = models.IntegerField(null=True)
    outlook_toronto = models.IntegerField(null=True)
    outlook_london = models.IntegerField(null=True)
    hamilton_niagra_low = models.IntegerField(null=True)
    hamilton_niagra_high = models.IntegerField(null=True)
    kingston_pembrooke_low = models.IntegerField(null=True)
    kingston_pembrooke_high = models.IntegerField(null=True)
    kitchener_waterloo_barrie_low = models.IntegerField(null=True)
    kitchener_waterloo_barrie_high = models.IntegerField(null=True)
    muskoka_kawartha_low = models.IntegerField(null=True)
    muskoka_kawartha_high = models.IntegerField(null=True)
    northeast_low = models.IntegerField(null=True)
    northeast_high = models.IntegerField(null=True)
    northwest_low = models.IntegerField(null=True)
    northwest_high = models.IntegerField(null=True)
    stratford_bruce_peninsula_low = models.IntegerField(null=True)    
    stratford_bruce_peninsula_high = models.IntegerField(null=True)    
    windsor_sarnia_low = models.IntegerField(null=True)
    windsor_sarnia_high = models.IntegerField(null=True)
    hamilton_niagra_outlook = models.IntegerField(null=True)
    kingston_pembrooke_outlook = models.IntegerField(null=True)
    kitchener_waterloo_barrie_outlook = models.IntegerField(null=True)
    muskoka_kawartha_outlook = models.IntegerField(null=True)
    northeast_outlook = models.IntegerField(null=True)
    northwest_outlook = models.IntegerField(null=True)  
    stratford_bruce_peninsula_outlook = models.IntegerField(null=True)  
    windsor_sarnia_outlook = models.IntegerField(null=True)     

    def toronto_range  (self):
        return self.torontohigh - self.toronto_low

    def __unicode__(self):
        string = self.name
        return string
    
    tor_range = property(toronto_range)

# Contains the prerequisite courses that are most likely to be inserted 
class Course(models.Model):
    name_english = models.CharField(max_length=64, blank=True)
    name_french = models.CharField(max_length=64, blank=True)
    mapping = models.ManyToManyField("Course", related_name="course_mapping")
    
    def __unicode__(self):
        string = self.code + " | " + self.name
        return string
        
    class Meta:
        ordering = ["name_english"]
        
   
class Prerequisite(models.Model):
    program = models.ForeignKey('Program', on_delete=models.CASCADE)
    course = models.ForeignKey('Course', on_delete=models.CASCADE, null=True)
    relation_type = models.CharField(max_length=64, choices=RELATION_TYPE, default="Alone", blank=True)
    group = models.IntegerField(null=True, default=0)
    #category = models.CharField(max_length=64, choices=COURSE_GROUP, blank=True, null=True, default='') # E.g Humanitarian, Math, Science
    

class Program(models.Model):
    name = models.CharField(max_length=255)
    school = models.CharField(max_length=255)
    school_code = models.CharField(max_length=64)
    city = models.CharField(max_length=45, blank=True)
    province = models.CharField(max_length=45, blank=True)
    degree = models.CharField(max_length=255, blank=True)
    #code = models.CharField(max_length=64, blank=True)
    domain = models.ForeignKey(Domain, blank=True, null=True, on_delete=models.SET_NULL)
    #duration = models.IntegerField()
    cost_per_year = models.IntegerField(null=True, default=0)
    tuition_link = models.CharField(max_length=255, blank=True)
    #coop = models.BooleanField(default=False)
    #french = models.BooleanField(default=False)
    # minimum admission average
    admission_avg = models.IntegerField(default=0)
    six_uni_required = models.BooleanField(verbose_name="Six university courses required", default=False)
    category_prereq = models.CharField("Fine Arts, Humanities, or Languages", max_length=62, blank=True, default=None, choices=RELATION_TYPE_2)
    prereqs_done = models.NullBooleanField(verbose_name="Prerequisites set", default=None)
    scholarships_avg = models.IntegerField(blank=True, default=None)
    notes = models.TextField("Extra notes", blank=True)
    logo_path = models.CharField(max_length=255, blank=True)
    site_link = models.CharField(max_length=255, blank=True)
    scholarship_link = models.CharField(max_length=255, blank=True)
    prereq_courses = models.ManyToManyField(Course, through=Prerequisite)
    lang_score = models.IntegerField(null=True, default=None)
    lang_link = models.CharField(max_length=255, blank=True)
    lang_test_link = models.CharField(max_length=255, blank=True)
    res_cost = models.IntegerField(null=True, default=None) #per two semesters
    food_cost = models.IntegerField(null=True, default=None) #per two semesters
    costs_link = models.CharField(max_length=255, blank=True)
    
    def __unicode__(self):  # Python 3: def __str__(self):
		#string = self.name + " - " + self.address + " - " + self.city + ", " + self.province
		#return string
        return self.name

class Country_Diploma(models.Model):
    country_name = models.CharField(max_length=255)
    country_code = models.CharField(max_length=2)
    school_diploma = models.CharField(max_length=255)


class Profile(AbstractBaseUser):
    #username = models.CharField(_("Username"), max_length=254, unique=True)
    uid = models.UUIDField(primary_key=False, default=uuid.uuid4, editable=False, unique=True)
    email = models.EmailField("Email address", max_length=254, unique=True )
    name = models.CharField(_("Name"), max_length=254)
    user_type = models.CharField("User Type", max_length=50, blank=False)
    gender = models.CharField("Gender", max_length=10, blank=False)
    date_birth = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True)
    grade = models.CharField(_("Grade"), max_length=50, blank=True, null=True)
    city = models.CharField("City", max_length=254, blank=True)
    promo_code = models.CharField(_("Promo Code"), max_length=254, blank=True)
    school_language = models.CharField(max_length=64, choices=SCHOOL_LANGUAGE, blank=False)
    country = models.ForeignKey(Country_Diploma, null=True, on_delete=models.SET_NULL)
    language = models.CharField("Language", max_length=254, choices=LANG, default="en")
    mother_tongue = models.CharField("Mother Tongue", max_length=50)
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField("is admin", default=False)
    date_registered = models.DateTimeField(auto_now_add=True, auto_now=False, null=True)
    careers = models.ManyToManyField(Career, null=True)
    programs = models.ManyToManyField(Program, null=True)
    paid = models.BooleanField(default=False)
    order_id = models.CharField(max_length=50, unique=True)

    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    #REQUIRED_FIELDS = [ ]

    def get_full_name(self):
        # The user is identified by their email address
        return self.username

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __unicode__(self):
        return "%s %s" % (self.name)

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True
        
    def meta(self):
        return self._meta

class Grades(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    summary_programs = models.IntegerField()
    summary_careers = models.IntegerField()
    summary_salary = models.IntegerField()
    domains = models.CharField(max_length=255, null=True)
    courses = models.CharField(max_length=255, null=True)
    average = models.CharField(max_length=50, null=True)
    course1 = models.CharField(max_length=50)
    course2 = models.CharField(max_length=50, null=True, blank=True)
    course3 = models.CharField(max_length=50, null=True, blank=True)
    course4 = models.CharField(max_length=50, null=True, blank=True)
    course5 = models.CharField(max_length=50, null=True, blank=True)
    course6 = models.CharField(max_length=50, null=True, blank=True)
    course7 = models.CharField(max_length=50, null=True, blank=True)
    course8 = models.CharField(max_length=50, null=True, blank=True)
    grade1 = models.IntegerField(null=True, blank=True)
    grade2 = models.IntegerField(null=True, blank=True)
    grade3 = models.IntegerField(null=True, blank=True)
    grade4 = models.IntegerField(null=True, blank=True)
    grade5 = models.IntegerField(null=True, blank=True)
    grade6 = models.IntegerField(null=True, blank=True)
    grade7 = models.IntegerField(null=True, blank=True)
    grade8 = models.IntegerField(null=True, blank=True)

		
class Scholarship(models.Model):
    name = models.CharField(max_length=255)
    school = models.CharField(max_length=255)
    amount = models.CharField(max_length=255)
    amount_num = models.IntegerField()
    min_avg = models.IntegerField(blank=True, default='')
    description = models.TextField(blank=True)
    link = models.CharField(max_length=255)


