from django.contrib import admin
from journey.models import Program, Course, Prerequisite, Profile, Grades
# Register your models here.


class PrereqInline(admin.TabularInline):
    model = Program.prereq_courses.through
    fields = ('relation_type', 'group', 'course', )
    
class GradesInline(admin.TabularInline):
    model = Grades
    
class ProgramAdmin(admin.ModelAdmin):
    model = Program
    list_display = ('id', 'name', 'school', 'degree', 'admission_avg',  'prereqs_done', 'domain' )
    ordering = ['id' ]
    search_fields = ('name', 'school', 'degree', )
    #filter_horizontal = ('prereq_courses',)
    exclude = ("prereq_courses", )
    inlines = (
       PrereqInline,
    )
    actions = ['mark_prerequisites_set', ]
    
    def mark_prerequisites_set(self, request, queryset):
        queryset.update(prereqs_done=True)
        self.message_user(request, "Selected programs were marked as prerequisites set True")
   
class CourseAdmin(admin.ModelAdmin):
    model = Course
    #list_display = ('id', 'code', 'name', 'category' )
    
class ProfileAdmin(admin.ModelAdmin):
    model = Profile
    #list_display = ('id', 'email', 'name', 'city', 'is_active', 'is_staff', 'date_registered' )
    inlines = (
       GradesInline,
    )
    
    
admin.site.register(Course, CourseAdmin)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(Program, ProgramAdmin)