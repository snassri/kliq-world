from django.shortcuts import render
from django.contrib.sites.shortcuts import get_current_site
from django.db import connection
from django.db.models import Count, Q
from django.http import HttpResponse, JsonResponse
from django.core import serializers
from django.core.mail import EmailMultiAlternatives
from django.views.decorators.csrf import csrf_exempt
from django.template import Context
from django.template.loader import get_template
from django.contrib.gis.geoip2 import GeoIP2
import json

from rest_framework import generics
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .serializers import GradesSerializer, SummarySerializer, Summary, CourseSerializer, ProgramSerializer, CareerSerializer

from journey.models import Program, Prerequisite, Course, Career, Scholarship, Career_Domain, Grades, Profile, Country_Diploma
from journey.forms import RegistrationForm, LoginForm

# API views
class UserData(generics.RetrieveAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = GradesSerializer

    def get(self, request):
        if request.user:
            curr_profile = Profile.objects.get(pk=request.user.id)
            try:
                grades = Grades.objects.get(profile=curr_profile)    
            except Grades.DoesNotExist:
                grades = None
            return Response(GradesSerializer(grades).data)

class GetSessionData(generics.GenericAPIView):
    serializer_class = SummarySerializer
    
    def get(self, request):
        if not request.user.is_authenticated and 'summaryPrograms' in request.session:
            programs_sum = request.session.get('summaryPrograms')
            careers_sum = request.session.get('summaryCareers')
            salary_sum = request.session.get('summarySalary')
            course1 = request.session.get('course1')
            course2 = request.session.get('course2')
            course3 = request.session.get('course3')
            course4 = request.session.get('course4')
            course5 = request.session.get('course5')
            course6 = request.session.get('course6')
            course7 = request.session.get('course7')
            course8 = request.session.get('course8')
            grade1 = request.session.get('grade1')
            grade2 = request.session.get('grade2')
            grade3 = request.session.get('grade3')
            grade4 = request.session.get('grade4')
            grade5 = request.session.get('grade5')
            grade6 = request.session.get('grade6')
            grade7 = request.session.get('grade7')
            grade8 = request.session.get('grade8')
            new_summary = Summary(programs=programs_sum, careers=careers_sum, salary=salary_sum, course1=course1, course2=course2, course3=course3, course4=course4, course5=course5, course6=course6, course7=course7, course8=course8, grade1=grade1, grade2=grade2, grade3=grade3, grade4=grade4, grade5=grade5, grade6=grade6, grade7=grade7, grade8=grade8)
            return Response(SummarySerializer(new_summary).data)
        else:
            return Response(None)
    
    
'''
# USER signed in: get all courses and user data
def get_user_data(request):
    
    num_programs = 0;
    num_careers = 0;
    salary = 0;
    
    # if user is signed in, get all their data
    if request.user.is_authenticated:
        courses_list = {}
        grades_list = {}
        summary_numbers = {}
        serialized_data = []
        
        if 'newreg' in request.session:
            if 'coursesdict' in request.session:
                courses_list = request.session.get('coursesdict')
            if 'gradesdict' in request.session:
                grades_list = request.session.get('gradesdict')
            if 'summarynumbers' in request.session:
                summary_numbers = request.session.get('summarynumbers')
            
            data = {
                'courseslist': courses_list,
                'gradeslist': grades_list,
                'summarynumbers' : summary_numbers,
                #'userdata' : serialized_data,
            }
        else:
            current_profile = Profile.objects.get(pk=request.user.id)
            user_data = Grades.objects.filter(profile=current_profile)
            serialized_data = serializers.serialize("json", user_data)
            data = {
                'userdata' : serialized_data,
            }
            
    else:
        serialized_data = None
    
    return JsonResponse(data)
'''

# Recieves courses and grades inputs and sends three summary numbers
@csrf_exempt
@api_view(('POST',))
def get_summary_data(request):
    
    data = {}
    messages = []
    average = 0
    captured_course_ids = []
    captured_course_names = []
    captured_grades = []
    num_programs = 0
    num_careers = 0
    median_salary = 0
    serialized_programs = {}
    serialized_careers = {}
    serialized_final_courses = {}
    program_domains = None

    if request.method == 'POST':
        
        try:
            for key, value in request.POST.items():
                if key.startswith('course') and key.endswith('[id]') and value != "":
                    captured_course_ids.append(int(value))
                if key.startswith('course') and not key.endswith('[name_english]') and not key.endswith('[name_french]') and not key.endswith('[id]') and value != "":
                    captured_course_names.append(value)
                if key.startswith('grade') and value:
                    captured_grades.append(value)
            
            #captured_courses.append("ENG4U")
            messages.append("captured course ids:")
            messages.append(captured_course_ids)
            messages.append("captured course names:")
            messages.append(captured_course_names)
            # saved captured results in session for later retreival
            request.session['courses'] = captured_course_ids
                
        except:
            messages.append('ERROR')
    
    # if captured_course_names (retrieved from DB) exist in Course table, save their ID's in captured_course_ids
    if len(captured_course_names) > 0:
        for course in captured_course_names:
            try:
                the_course = Course.objects.get(name_english=course)
            except Course.DoesNotExist:
                continue
            else:
                captured_course_ids.append(the_course.pk)
        
    if len(captured_course_ids) < 1 and len(captured_course_names) >= 1:
        # if at least one grade is captured, calculate average
        if len(captured_grades) > 0:
            # convert grades list to ints
            captured_grades = list(map(int, captured_grades))
            average = sum(captured_grades) / len(captured_grades) # sum(captured_grades) is throwing an error
        else:
            average = 0
        # save average in cache for later use
        request.session["average"] = average

        # get all programs with elig. average and have no prerequisites
        eligible_programs = Program.objects.filter(admission_avg__lte=average,prerequisite=None).distinct()
        
    if len(captured_course_ids) >= 1:        
        
        # if at least one grade is captured, calculate average
        if len(captured_grades) > 0:
            # convert grades list to ints
            captured_grades = list(map(int, captured_grades))
            average = sum(captured_grades) / len(captured_grades) # sum(captured_grades) is throwing an error
        else:
            average = 0
        # save average in cache for later use
        request.session["average"] = average
        
        taken_courses = Course.objects.filter(pk__in=captured_course_ids)
        # All courses that the student has taken PLUS the grade 12 courses that are mapped to from grade 9-11 courses
        final_courses = Course.objects.filter( Q(pk__in=captured_course_ids) | Q(course_mapping__in=taken_courses) ).distinct()
        serialized_final_courses = serializers.serialize("json", final_courses)
        final_course_ids = final_courses.values_list("id", flat=True)
        final_course_ids = list(final_course_ids)

        with connection.cursor() as cursor:
            sql="select id From (Select p3.id1, SUM(p3.numGroups) as numGroupsTaken From ( (select T3.id1, T3.numGroups from (select T1.id as id1, T1.name, COUNT(T1.course_id) as numGroups FROM (Select journey_program.id, journey_program.name, journey_prerequisite.group, journey_prerequisite.course_id FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id JOIN journey_course on journey_course.id = journey_prerequisite.course_id where journey_prerequisite.relation_type = 'Alone' and journey_course.id IN (%s) ) T1 GROUP BY T1.id) T3 JOIN (select T2.id as id2, T2.name, COUNT(T2.course_id) as numGroups FROM (Select DISTINCT journey_program.id, journey_program.name, journey_prerequisite.group, journey_prerequisite.course_id FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id JOIN journey_course on journey_course.id = journey_prerequisite.course_id where journey_prerequisite.relation_type = 'Alone') T2 GROUP BY T2.id) T4 ON T3.id1=T4.id2 and T3.numGroups  = T4.numGroups ) UNION ALL (select T3.id1, T3.numGroups from (select T1.id as id1, T1.name, COUNT(T1.group) as numGroups FROM (Select DISTINCT  journey_program.id, journey_program.name, journey_prerequisite.group FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id JOIN journey_course on journey_course.id = journey_prerequisite.course_id where journey_prerequisite.relation_type = 'One of' and journey_course.id IN (%s) ) T1 GROUP BY T1.id) T3 JOIN (select T2.id as id2, T2.name, COUNT(T2.group) as numGroups FROM (Select DISTINCT journey_program.id, journey_program.name, journey_prerequisite.group FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id JOIN journey_course on journey_course.id = journey_prerequisite.course_id where journey_prerequisite.relation_type = 'One of') T2 GROUP BY T2.id) T4 ON T3.id1=T4.id2 and T3.numGroups = T4.numGroups) UNION ALL (select T5.id1, T5.numGroups from (select T3.id as id1, T3.name, COUNT(T3.group) as numGroups From (select T1.id, T1.name, T1.group, COUNT(T1.course_id) as countCourses FROM (Select DISTINCT  journey_program.id, journey_program.name, journey_prerequisite.group, journey_prerequisite.course_id FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id JOIN journey_course on journey_course.id = journey_prerequisite.course_id where journey_prerequisite.relation_type = 'Two of' and journey_course.id IN (%s)  ) T1 GROUP BY T1.group having countCourses  >= 2) T3 GROUP BY T3.id) T5 JOIN (select T2.id as id2, T2.name, COUNT(T2.group) as numGroups FROM (Select DISTINCT journey_program.id, journey_program.name, journey_prerequisite.group FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id JOIN journey_course on journey_course.id = journey_prerequisite.course_id where journey_prerequisite.relation_type = 'Two of') T2 GROUP BY T2.id) T4 ON T5.id1=T4.id2 and T5.numGroups = T4.numGroups))p3 GROUP BY p3.id1) pFinal1 JOIN ( Select p31.id, SUM(p31.numGroups) as numGroupsNeeded From ( (select p11.id, p11.name, COUNT(p11.group) as numGroups from (Select journey_program.id, journey_program.name, journey_prerequisite.group FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id where journey_prerequisite.relation_type = 'Alone')p11 Group By p11.id) UNION ALL (select p11.id, p11.name, COUNT(p11.group) as numGroups from (Select distinct journey_program.id, journey_program.name, journey_prerequisite.group FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id where journey_prerequisite.relation_type <> 'Alone')p11 Group By p11.id))p31 GROUP BY p31.id) pFinal2 on pFinal1.id1 = pFinal2.id AND pFinal1.numGroupsTaken = pFinal2.numGroupsNeeded"   
            in_p=', '.join(map(lambda x: '%s', final_course_ids))
            sql = sql % (in_p, in_p, in_p)
            #cursor.execute(sql)
            #sql = sql % ('%s', in_p)
            params = []
            #params.append(type)
            params.extend(final_course_ids)
            params.extend(final_course_ids)
            params.extend(final_course_ids)
            cursor.execute(sql, tuple(params))
            # See this for explanation: http://grokbase.com/t/gg/django-users/13bgdpp51y/problem-with-raw-query-and-using-in 
            program_ids = [row[0] for row in cursor.fetchall()]
            #messages.append(connection.queries)
    
        # filter these programs further based on average and add programs with no prereqs
        eligible_programs = Program.objects.filter( Q(admission_avg__lte=average,id__in=program_ids) | Q(admission_avg__lte=average,prerequisite=None) ).distinct()
            
    try:
        num_programs = eligible_programs.count()
        serialized_programs = json.dumps( [{'pk': c.id, 'name': c.name} for c in eligible_programs] )
    except:
        num_programs=0

    ### Careers ###
    # First: get list of all unique domains of filtered programs
    try:
        program_domains = eligible_programs.values_list('domain', flat=True).distinct()
        program_domains = list(program_domains)
        request.session['domains'] = program_domains
    except:
        messages.append("Error getting values list of program_domains")

    # Second: get all the careers under the same domain
    try:
        filtered_careers = Career.objects.filter(domain__in=program_domains).distinct()
        num_careers = filtered_careers.count()
        serialized_careers = json.dumps( [{'name': c.name, 'income': c.income} for c in filtered_careers] )
    except:
        messages.append("something wrong on getting filtered_careers")
    else:
        ### Average Salary ###
        # Get the median salary out of the filtered careers. See: http://stackoverflow.com/questions/942620/missing-median-aggregate-function-in-django
        if num_careers >= 1:
            try:
                median_salary = filtered_careers.values_list('income', flat=True).order_by('income')[int(round(num_careers/2))]
            except:
                messages.append("something wrong when getting median_salary")

    data = {
        'messages': messages,
        'average': average, 
        'programs': serialized_programs,
        'domains': program_domains,
        'num_programs': num_programs,
        'careers': serialized_careers,
        'num_careers': num_careers,
        'salary': median_salary,
        'courses': serialized_final_courses,
        #'type' : str(type(filtered_careers))
    }
    
    return JsonResponse(data)

# Save courses and grades in session for new users
@csrf_exempt
def return_captured_data(request):
    
    captured_courses = {}
    captured_grades = {}
    captured_summary = {}
    
    if request.method == 'POST':
        
        if 'summaryPrograms' in request.POST:
            summary_programs = request.POST.get('summaryPrograms', None)
            captured_summary['summaryPrograms'] = summary_programs
        if 'summaryCareers' in request.POST:
            summary_careers = request.POST.get('summaryCareers', None)
            captured_summary['summaryCareers'] = summary_careers
        if 'summarySalary' in request.POST:
            summary_salary = request.POST.get('summarySalary', None)
            captured_summary['summarySalary'] = summary_salary
        
        course1 = course2 = course3 = course4 = course5 = course6 = course7 = course8 = None
        
        if 'course1[name_english]' in request.POST:
            course1 = request.POST.get('course1[name_english]', None)
        elif 'course1' in request.POST:
            course1 = request.POST.get('course1', None)
        
        if 'course2[name_english]' in request.POST:
            course2 = request.POST.get('course2[name_english]', None)
        elif 'course2' in request.POST:
            course2 = request.POST.get('course2', None)
            
        if 'course3[name_english]' in request.POST:
            course3 = request.POST.get('course3[name_english]', None)
        elif 'course3' in request.POST:
            course3 = request.POST.get('course3', None)
            
        if 'course4[name_english]' in request.POST:
            course4 = request.POST.get('course4[name_english]', None)
        elif 'course4' in request.POST:
            course4 = request.POST.get('course4', None)
            
        if 'course5[name_english]' in request.POST:
            course5 = request.POST.get('course5[name_english]', None)
        elif 'course5' in request.POST:
            course5 = request.POST.get('course5', None)
            
        if 'course6[name_english]' in request.POST:
            course6 = request.POST.get('course6[name_english]', None)
        elif 'course6' in request.POST:
            course6 = request.POST.get('course6', None)

        if 'course7[name_english]' in request.POST:
            course7 = request.POST.get('course7[name_english]', None)
        elif 'course7' in request.POST:
            course7 = request.POST.get('course7', None)
            
        if 'course8[name_english]' in request.POST:
            course8 = request.POST.get('course8[name_english]', None)
        elif 'course8' in request.POST:
            course8 = request.POST.get('course8', None)
        
        captured_courses['course1'] = course1
        captured_courses['course2'] = course2
        captured_courses['course3'] = course3
        captured_courses['course4'] = course4
        captured_courses['course5'] = course5
        captured_courses['course6'] = course6
        captured_courses['course7'] = course7
        captured_courses['course8'] = course8
        
        grade1 = request.POST.get('grade1', None)
        grade2 = request.POST.get('grade2', None)
        grade3 = request.POST.get('grade3', None)
        grade4 = request.POST.get('grade4', None)
        grade5 = request.POST.get('grade5', None)
        grade6 = request.POST.get('grade6', None)
        grade7 = request.POST.get('grade7', None)
        grade8 = request.POST.get('grade8', None)
        
        if grade1:
            grade1 = int(grade1)
        else: 
            grade1 = None
        if grade2:
            grade2 = int(grade2)
        else:
            grade2 = None
        if grade3:
            grade3 = int(grade3)
        else:
            grade3 = None
        if grade4:
            grade4 = int(grade4)
        else:
            grade4 = None
        if grade5: 
            grade5 = int(grade5)
        else:
            grade5 = None
        if grade6:
            grade6 = int(grade6)
        else:
            grade6 = None
        if grade7: 
            grade7 = int(grade7)
        else:
            grade7 = None
        if grade8:
            grade8 = int(grade8)
        else:
            grade8 = None
        
        captured_grades['grade1'] = grade1
        captured_grades['grade2'] = grade2
        captured_grades['grade3'] = grade3
        captured_grades['grade4'] = grade4
        captured_grades['grade5'] = grade5
        captured_grades['grade6'] = grade6
        captured_grades['grade7'] = grade7
        captured_grades['grade8'] = grade8
            
    data = {
        'courses': captured_courses,
        'grades': captured_grades,
        'summary': captured_summary
    }
    
    return data
    
# Save courses and grades in session for new users
@api_view(('POST',))
def save_session_data(request):
    
    # delete all session variables to save new ones except a few
    # request.session.flush()
    for key in list(request.session.keys()):
        if(key != 'domains' and key != 'average' and key != 'courses'):
            del request.session[key]
        
    data = return_captured_data(request)
    courses = data["courses"]
    grades = data["grades"]
    summary = data["summary"]
    
    summary_programs = int(summary['summaryPrograms'])
    summary_careers = int(summary['summaryCareers'])
    summary_salary = int(summary['summarySalary'])
    
    # save new session variables
    request.session["summaryPrograms"] = summary_programs
    request.session["summaryCareers"] = summary_careers
    request.session["summarySalary"] = summary_salary
    
    if courses['course1']:
        request.session['course1'] = courses['course1']
    if courses['course2']:
        request.session['course2'] = courses['course2']
    if courses['course3']:
        request.session['course3'] = courses['course3']
    if courses['course4']:
        request.session['course4'] = courses['course4']
    if courses['course5']:
        request.session['course5'] = courses['course5']
    if courses['course6']:
        request.session['course6'] = courses['course6']
    if courses['course7']:
        request.session['course7'] = courses['course7']
    if courses['course8']:
        request.session['course8'] = courses['course8']
        
    request.session['grade1'] = grades['grade1']
    request.session['grade2'] = grades['grade2']
    request.session['grade3'] = grades['grade3']
    request.session['grade4'] = grades['grade4']
    request.session['grade5'] = grades['grade5']
    request.session['grade6'] = grades['grade6']
    request.session['grade7'] = grades['grade7']
    request.session['grade8'] = grades['grade8']
            
    data = {
        "courses": courses,
        "grades": grades,
        "programs": summary['summaryPrograms'],
        "careers": summary['summaryCareers'],
        "salary": summary['summarySalary'],
        "domains": request.session['domains'],
    }
    
    return Response(data)
    
 
 # USER signed in: save user grades in DB (called when user is signed in only)
@api_view(('POST',))
def save_user_data(request):
    
    messages = []
    captured_courses = {}
    captured_grades = {}
    
    if request.user.is_authenticated:
        current_profile = Profile.objects.get(pk=request.user.id)
    
    if request.method == 'POST':
        
        data = return_captured_data(request)
        captured_courses = data['courses']
        captured_grades = data['grades']
        captured_summary = data['summary']
        course_ids = []
        grades = []
        
        for key, value in captured_courses.items():
            try:
                the_course = Course.objects.get(name_english=value)
            except Course.DoesNotExist:
                continue
            else:
                course_ids.append(the_course.pk)      
                
        final_courses = Course.objects.filter(pk__in=course_ids)
        serialized_final_courses = serializers.serialize("json", final_courses)
        final_course_ids = final_courses.values_list("id", flat=True)
        final_course_ids = list(final_course_ids)

        with connection.cursor() as cursor:
            sql="select id From (Select p3.id1, SUM(p3.numGroups) as numGroupsTaken From ( (select T3.id1, T3.numGroups from (select T1.id as id1, T1.name, COUNT(T1.course_id) as numGroups FROM (Select journey_program.id, journey_program.name, journey_prerequisite.group, journey_prerequisite.course_id FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id JOIN journey_course on journey_course.id = journey_prerequisite.course_id where journey_prerequisite.relation_type = 'Alone' and journey_course.id IN (%s) ) T1 GROUP BY T1.id) T3 JOIN (select T2.id as id2, T2.name, COUNT(T2.course_id) as numGroups FROM (Select DISTINCT journey_program.id, journey_program.name, journey_prerequisite.group, journey_prerequisite.course_id FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id JOIN journey_course on journey_course.id = journey_prerequisite.course_id where journey_prerequisite.relation_type = 'Alone') T2 GROUP BY T2.id) T4 ON T3.id1=T4.id2 and T3.numGroups  = T4.numGroups ) UNION ALL (select T3.id1, T3.numGroups from (select T1.id as id1, T1.name, COUNT(T1.group) as numGroups FROM (Select DISTINCT  journey_program.id, journey_program.name, journey_prerequisite.group FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id JOIN journey_course on journey_course.id = journey_prerequisite.course_id where journey_prerequisite.relation_type = 'One of' and journey_course.id IN (%s) ) T1 GROUP BY T1.id) T3 JOIN (select T2.id as id2, T2.name, COUNT(T2.group) as numGroups FROM (Select DISTINCT journey_program.id, journey_program.name, journey_prerequisite.group FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id JOIN journey_course on journey_course.id = journey_prerequisite.course_id where journey_prerequisite.relation_type = 'One of') T2 GROUP BY T2.id) T4 ON T3.id1=T4.id2 and T3.numGroups = T4.numGroups) UNION ALL (select T5.id1, T5.numGroups from (select T3.id as id1, T3.name, COUNT(T3.group) as numGroups From (select T1.id, T1.name, T1.group, COUNT(T1.course_id) as countCourses FROM (Select DISTINCT  journey_program.id, journey_program.name, journey_prerequisite.group, journey_prerequisite.course_id FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id JOIN journey_course on journey_course.id = journey_prerequisite.course_id where journey_prerequisite.relation_type = 'Two of' and journey_course.id IN (%s)  ) T1 GROUP BY T1.group having countCourses  >= 2) T3 GROUP BY T3.id) T5 JOIN (select T2.id as id2, T2.name, COUNT(T2.group) as numGroups FROM (Select DISTINCT journey_program.id, journey_program.name, journey_prerequisite.group FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id JOIN journey_course on journey_course.id = journey_prerequisite.course_id where journey_prerequisite.relation_type = 'Two of') T2 GROUP BY T2.id) T4 ON T5.id1=T4.id2 and T5.numGroups = T4.numGroups))p3 GROUP BY p3.id1) pFinal1 JOIN ( Select p31.id, SUM(p31.numGroups) as numGroupsNeeded From ( (select p11.id, p11.name, COUNT(p11.group) as numGroups from (Select journey_program.id, journey_program.name, journey_prerequisite.group FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id where journey_prerequisite.relation_type = 'Alone')p11 Group By p11.id) UNION ALL (select p11.id, p11.name, COUNT(p11.group) as numGroups from (Select distinct journey_program.id, journey_program.name, journey_prerequisite.group FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id where journey_prerequisite.relation_type <> 'Alone')p11 Group By p11.id))p31 GROUP BY p31.id) pFinal2 on pFinal1.id1 = pFinal2.id AND pFinal1.numGroupsTaken = pFinal2.numGroupsNeeded" 
            in_p=', '.join(map(lambda x: '%s', final_course_ids))
            sql = sql % (in_p, in_p, in_p)
            #cursor.execute(sql)
            #sql = sql % ('%s', in_p)
            params = []
            #params.append(type)
            params.extend(final_course_ids)
            params.extend(final_course_ids)
            params.extend(final_course_ids)
            cursor.execute(sql, tuple(params))
            # See this for explanation: http://grokbase.com/t/gg/django-users/13bgdpp51y/problem-with-raw-query-and-using-in 
            program_ids = [row[0] for row in cursor.fetchall()]

        for key, value in captured_grades.items():
            if value is not None:
                grades.append(int(value))

            
        grades = list(map(int, grades))
        grades_average = sum(grades) / len(grades) # sum(grades) is throwing an error
        
        eligible_programs = Program.objects.filter( Q(admission_avg__lte=grades_average,id__in=program_ids) | Q(admission_avg__lte=grades_average,prerequisite=None) ).distinct()
        
        program_domains = eligible_programs.values_list('domain', flat=True).distinct()
        program_domains = list(program_domains)
        program_domains = json.dumps(program_domains)
        
        sprograms = captured_summary['summaryPrograms']
        scareers = captured_summary['summaryCareers']
        ssalary = captured_summary['summarySalary']
        course1 = captured_courses['course1']
        course2 = captured_courses['course2']
        course3 = captured_courses['course3']
        course4 = captured_courses['course4']
        course5 = captured_courses['course5']
        course6 = captured_courses['course6']
        course7 = captured_courses['course7']
        course8 = captured_courses['course8']
        grade1 = captured_grades['grade1']
        grade2 = captured_grades['grade2']
        grade3 = captured_grades['grade3']
        grade4 = captured_grades['grade4']
        grade5 = captured_grades['grade5']
        grade6 = captured_grades['grade6']
        grade7 = captured_grades['grade7']
        grade8 = captured_grades['grade8']
        
        #sprograms = eligible_programs.count()
        sprograms = request.POST.get('summaryPrograms', None)
        scareers = request.POST.get('summaryCareers', None)
        ssalary = request.POST.get('summarySalary', None)
            
        if request.user.is_authenticated:
            # if a row already exists in grades table, then update; otherwise create a new row
            try:
                Grades.objects.get(profile = current_profile)
            except Grades.DoesNotExist:
                new_grades = Grades(course1=course1, course2=course2, course3=course3, course4=course4, course5=course5, course6=course6, course7=course7, course8=course8,
                        grade1=grade1, grade2=grade2, grade3=grade3, grade4=grade4, grade5=grade5, grade6=grade6, grade7=grade7, grade8=grade8,
                        summary_programs=sprograms, summary_careers=scareers, summary_salary=ssalary, domains=program_domains, courses=course_ids, average=grades_average,
                        profile=current_profile)
              
                new_grades.save()
                messages.append("created new user data")  
            else:
                current_grades = Grades.objects.filter(profile = current_profile)
                current_grades = current_grades.update(course1=course1, course2=course2, course3=course3, course4=course4, course5=course5, course6=course6, course7=course7, course8=course8,
                    grade1=grade1, grade2=grade2, grade3=grade3, grade4=grade4, grade5=grade5, grade6=grade6, grade7=grade7, grade8=grade8,
                    summary_programs = sprograms, summary_careers = scareers, summary_salary=ssalary, domains=program_domains, courses=course_ids, average=grades_average,
                    profile=current_profile)
                messages.append("updated user data")
                
    data = {
            'messages': messages,
            'courses': serialized_final_courses,
        }
    
    return Response(data)

# USER & GUEST: Receives selected personality type and filters careers further and sends them
def return_personality_careers(request):
    
    messages = []
    serialized_careers = []
    domains = []
    loggedin = False
    
    if request.user.is_authenticated:
        user_info = Grades.objects.get(profile__pk=request.user.id)
        program_domains = user_info.domains
        jsonDec = json.decoder.JSONDecoder()
        program_domains = jsonDec.decode(program_domains)
		loggedin = True
    else:
        program_domains = request.session.get('domains')
   
    messages.append(program_domains)
    
    if request.is_ajax():
        if request.method == 'POST':
            personality_input = request.POST.get("personality")
    
            careers = Career.objects.filter(domain__in=program_domains).distinct().order_by('?')
            if personality_input != "No":
                careers = careers.filter(personality__code=personality_input).order_by('?')
                
            serialized_careers = serializers.serialize("json", careers)
           
    
    data = {
        'careers' : serialized_careers,
        'personality' : personality_input,
        'messages': messages,
        'domains': program_domains,
		'loggedin': loggedin
    }
    
    return data

@api_view(('POST',))
def get_personality_careers(request):
    
    data = return_personality_careers(request)
    return Response(data)

@csrf_exempt
def update_results(request):
    
    # commented for beta
    #save_user_data(request)
    data = return_personality_careers(request)
    
    return JsonResponse(data)

@api_view(('POST',))
def add_career(request):
    
    if request.is_ajax():
        if request.method == 'POST':
            career = request.POST.get("career")
            the_career = Career.objects.get(pk=career)
            
            if request.user.is_authenticated:
                current_profile = Profile.objects.get(pk=request.user.id)
                current_profile.careers.add(the_career)
                
    data = {}
    
    return Response(data)

@api_view(('POST',))   
def remove_career(request):
    
    serialized_careers = []
    
    if request.is_ajax():
        if request.method == 'POST':
            career = request.POST.get("career")
            the_career = Career.objects.get(pk=career)
            
            if request.user.is_authenticated:
                current_profile = Profile.objects.get(pk=request.user.id)
                current_profile.careers.remove(the_career)
                careers = current_profile.careers.all()
                serialized_careers = serializers.serialize("json", careers)
    
    data = {
        'careers' : serialized_careers,
    }
                
    return Response(data)

    
# USER & GUEST: Recieves selected careers and sends related programs
@api_view(('POST',))
def get_programs(request):
    
    messages = []
    serialized_programs = []
    num_programs = 0
    serialized_scholarships = []

    if request.is_ajax():
        if request.method == 'POST':
            
            # get selected career domains
            selected_career = request.POST.get('careerid')
            career_domains = Career_Domain.objects.filter(career_id=selected_career).order_by("level").distinct()
            domains_list = career_domains.values_list("domain_id", flat=True)
            domains_list = list(domains_list)
            messages.append(domains_list)
            
            # Recalculate the programs based on inputted courses and grades
            if request.user.is_authenticated:
                user_info = Grades.objects.get(profile__pk=request.user.id)
                captured_courses = user_info.courses
                jsonDec = json.decoder.JSONDecoder()
                captured_courses = jsonDec.decode(captured_courses)
                average = float(user_info.average)
            else:
                captured_courses = request.session.get('courses')
                average = int(request.session.get('average'))
                #convert capture courses ids to ints
                captured_courses = list(map(int, captured_courses))
                
            messages.append("captured courses:")
            messages.append(captured_courses)
            
            taken_courses = Course.objects.filter(pk__in=captured_courses)
            final_courses = Course.objects.filter( Q(pk__in=captured_courses) | Q(course_mapping__in=taken_courses) ).distinct()
            #not_taken_courses = Course.objects.exclude(code__in=final_courses_ids)
            serialized_final_courses = serializers.serialize("json", final_courses)
            final_course_ids = final_courses.values_list("id", flat=True)
            final_course_ids = list(final_course_ids)
        
            with connection.cursor() as cursor:
                sql="select id From (Select p3.id1, SUM(p3.numGroups) as numGroupsTaken From ( (select T3.id1, T3.numGroups from (select T1.id as id1, T1.name, COUNT(T1.course_id) as numGroups FROM (Select journey_program.id, journey_program.name, journey_prerequisite.group, journey_prerequisite.course_id FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id JOIN journey_course on journey_course.id = journey_prerequisite.course_id where journey_prerequisite.relation_type = 'Alone' and journey_course.id IN (%s) ) T1 GROUP BY T1.id) T3 JOIN (select T2.id as id2, T2.name, COUNT(T2.course_id) as numGroups FROM (Select DISTINCT journey_program.id, journey_program.name, journey_prerequisite.group, journey_prerequisite.course_id FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id JOIN journey_course on journey_course.id = journey_prerequisite.course_id where journey_prerequisite.relation_type = 'Alone') T2 GROUP BY T2.id) T4 ON T3.id1=T4.id2 and T3.numGroups  = T4.numGroups ) UNION ALL (select T3.id1, T3.numGroups from (select T1.id as id1, T1.name, COUNT(T1.group) as numGroups FROM (Select DISTINCT  journey_program.id, journey_program.name, journey_prerequisite.group FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id JOIN journey_course on journey_course.id = journey_prerequisite.course_id where journey_prerequisite.relation_type = 'One of' and journey_course.id IN (%s) ) T1 GROUP BY T1.id) T3 JOIN (select T2.id as id2, T2.name, COUNT(T2.group) as numGroups FROM (Select DISTINCT journey_program.id, journey_program.name, journey_prerequisite.group FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id JOIN journey_course on journey_course.id = journey_prerequisite.course_id where journey_prerequisite.relation_type = 'One of') T2 GROUP BY T2.id) T4 ON T3.id1=T4.id2 and T3.numGroups = T4.numGroups) UNION ALL (select T5.id1, T5.numGroups from (select T3.id as id1, T3.name, COUNT(T3.group) as numGroups From (select T1.id, T1.name, T1.group, COUNT(T1.course_id) as countCourses FROM (Select DISTINCT  journey_program.id, journey_program.name, journey_prerequisite.group, journey_prerequisite.course_id FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id JOIN journey_course on journey_course.id = journey_prerequisite.course_id where journey_prerequisite.relation_type = 'Two of' and journey_course.id IN (%s)  ) T1 GROUP BY T1.group having countCourses  >= 2) T3 GROUP BY T3.id) T5 JOIN (select T2.id as id2, T2.name, COUNT(T2.group) as numGroups FROM (Select DISTINCT journey_program.id, journey_program.name, journey_prerequisite.group FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id JOIN journey_course on journey_course.id = journey_prerequisite.course_id where journey_prerequisite.relation_type = 'Two of') T2 GROUP BY T2.id) T4 ON T5.id1=T4.id2 and T5.numGroups = T4.numGroups))p3 GROUP BY p3.id1) pFinal1 JOIN ( Select p31.id, SUM(p31.numGroups) as numGroupsNeeded From ( (select p11.id, p11.name, COUNT(p11.group) as numGroups from (Select journey_program.id, journey_program.name, journey_prerequisite.group FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id where journey_prerequisite.relation_type = 'Alone')p11 Group By p11.id) UNION ALL (select p11.id, p11.name, COUNT(p11.group) as numGroups from (Select distinct journey_program.id, journey_program.name, journey_prerequisite.group FROM journey_program JOIN journey_prerequisite ON journey_program.id = journey_prerequisite.program_id where journey_prerequisite.relation_type <> 'Alone')p11 Group By p11.id))p31 GROUP BY p31.id) pFinal2 on pFinal1.id1 = pFinal2.id AND pFinal1.numGroupsTaken = pFinal2.numGroupsNeeded"   
                in_p=', '.join(map(lambda x: '%s', final_course_ids))
                sql = sql % (in_p, in_p, in_p)
                params = []
                params.extend(final_course_ids)
                params.extend(final_course_ids)
                params.extend(final_course_ids)
                cursor.execute(sql, tuple(params))
                program_ids = [row[0] for row in cursor.fetchall()]
            
            # filter these programs further based on average and add programs with no prereqs
            eligible_programs = Program.objects.filter( Q(admission_avg__lte=average,id__in=program_ids) | Q(admission_avg__lte=average,prerequisite=None) ).distinct()
            
            new_programs = eligible_programs.filter(domain__in=domains_list).order_by("domain")
            num_programs = new_programs.count()
            #serialized_programs = serializers.serialize("json", new_programs)
            serialized_programs = ProgramSerializer(new_programs, many=True)

            try:
                scholarships = Scholarship.objects.all()
                serialized_scholarships = serializers.serialize("json", scholarships)
            except:
                messages.append('Error filtering scholarships')
    
    data = {
        'programs' : serialized_programs.data,
        'num_programs': num_programs,
        'messages' : messages,
        'scholarships' : serialized_scholarships,
        'courses' : serialized_final_courses,
    }
    
    return Response(data)
     
@api_view(['GET'])
def get_user_careers(request):
    
    the_profile = Profile.objects.get(pk=request.user.id)
    careers = the_profile.careers.all()
    serialized_careers = CareerSerializer(careers, many=True)
    
    return Response(serialized_careers.data)
    
    
def send_feedback(request):
    
    user_name = request.POST.get('username')
    user_email = request.POST.get('useremail')
    feedback_1 = request.POST.get('firstq')
    feedback_2 = request.POST.get('secondq')
    
    plaintext = get_template('email_templates/feedback_response.txt')
    htmly     = get_template('email_templates/feedback_response.html')
    current_site = get_current_site(request)
    d = Context({ 'user_name': user_name, 'user_email':user_email, 'feedback_1': feedback_1, 'feedback_2': feedback_2 })

    subject, from_email, to_email = "Feedback Received", 'info@kliq.com', 'salma.nassri@gmail.com' 
    text_content = plaintext.render(d)
    html_content = htmly.render(d)
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to_email])
    msg.attach_alternative(html_content, "text/html")
    msg.send()
     
    data = {}
    
    return JsonResponse(data)
    

@api_view(('POST',))
def check_email_exists(request):
    
    email = request.POST.get('email')
    
    if Profile.objects.filter(email=email).exists():
        status = "not good"
    else:
        status = "good"
    
    data = {
        'emailexists' : status,
    }
    
    return JsonResponse(data)

# get the user's country from their IP
def get_user_country(request):
    ip = None
    if 'HTTP_X_FORWARDED_FOR' in request.META:
        ip = request.META['HTTP_X_FORWARDED_FOR'].split(",")[0].strip()
        request.META['REMOTE_ADDR'] = ip
    
    try:
        g = GeoIP2()
        country = g.country(ip)
        diploma = None

        if country:
            # get diploma required by country
            diploma = Country_Diploma.objects.get(country_code=country["coutry_code"])
            diploma = diploma.school_diploma

        data = {
            'state': 'success',
            'ip': ip,
            'other': request.META['REMOTE_ADDR'],
            'country': country,
            'diploma':  diploma
        }
    except:
        data = {
        'state': 'fail',
    }    

    return JsonResponse(data)


def get_diploma_from_country(request):
    
    captured_country = request.GET.get('country')
    captured_country = int(captured_country)
    diploma = Country_Diploma.objects.get(pk=captured_country)
    
    data = {
        'diploma': diploma.school_diploma,
    }    

    return JsonResponse(data)
