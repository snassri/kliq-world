from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from journey.models import Program, Prerequisite, Profile, Grades, Country_Diploma
#from django.contrib.auth.forms import AuthenticationForm
from journey.forms import RegistrationForm, LoginForm
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from django.utils import translation
from django.utils.translation import ugettext as _
from django.utils.crypto import get_random_string
from django.views.decorators.csrf import csrf_exempt
from paypal.standard.forms import PayPalPaymentsForm
import requests
import json

# the function that deals with sending emails
'''
def send_email(title, template, profile):
    
    plaintext = get_template('email_templates/'+template+'.txt')
    htmly     = get_template('email_templates/'+template+'.html')
    d = Context({ 'name': profile.name })

    subject, from_email, to_email = title, 'info@kliq.com', profile.email 
    text_content = plaintext.render(d)
    html_content = htmly.render(d)
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to_email])
    msg.attach_alternative(html_content, "text/html")
    msg.send()
'''
'''
# Paypal oAuth details
OAUTH_CLIENT_ID = "ARiJpzoJdpdgwmrwgheU7FFvlYb_jpBUjk44mz1DhkOKZOPHP_Fo3NX9Q9DYrw0O22GBBwLjjA6GdonB"
OAUTH_SECRET = "EPy5Brw2bTNvcSOJ-8mAN_LTResRzbP_cmwjj9mFfIOyr2RlUszxuna_xEKi2PHaRESUjYg4zpdMrvs7"

# Get Paypal access token
def get_access_token():
    url = 'https://api.sandbox.paypal.com/v1/oauth2/token'
    headers = {'Accept': 'application/json', 'Accept-Language': 'en_US'}
    data = {'grant_type': 'client_credentials'}
    auth = (OAUTH_CLIENT_ID, OAUTH_SECRET)

    response = requests.post(url, headers=headers, data=data, auth=auth)
    data = response.json()
    access_token = data['access_token']
'''

@csrf_exempt
def payment_done(request):
    orderid = request.session.get('order_id')
    user = Profile.objects.get(order_id=orderid)
    if user is not None:
        user.paid = True
        user.save()
                
        # Send activation email
        current_site = get_current_site(request)
        d = {'name': user.name, 'domain': current_site.domain, 'uid': urlsafe_base64_encode(
            force_bytes(user.pk)).decode(), 'token': account_activation_token.make_token(user), }

        subject, from_email, to_email = _(
            "Activate your KLIQ.CA account"), 'info@kliq.com', user.email
        # text_content = get_template('email_templates/signup_activation.txt').render(d)
        html_content = get_template(
            'email_templates/signup_activation.html').render(d)
        msg = EmailMultiAlternatives(
            subject, html_content, from_email, [to_email])
        msg.attach_alternative(html_content, "text/html")
        msg.send()      

    return render(request, "payment/done.html")

@csrf_exempt
def payment_cancelled(request):
    return render(request, "payment/cancel.html")

def payment_process(request):
    order_id = request.session.get('order_id')

    paypal_dict = {
        "business": settings.PAYPAL_RECEIVER_EMAIL,
        "amount": "229.00",
        "currency_code": "CAD",  
        "item_name": "Kliq World Subscription",
        "invoice": str(order_id),
        "notify_url": request.build_absolute_uri(reverse('paypal-ipn')),
        "return": request.build_absolute_uri(reverse('payment_done')),
        "cancel_return": request.build_absolute_uri(reverse('payment_cancelled')),
    }

    form = PayPalPaymentsForm(initial=paypal_dict)
    return render(request, "payment/process.html", {'form': form })


def activate_language(lang=None):
    language = lang
    if language == "fr" or language == "French":
        translation.activate("fr")
        language = "fr"
    else:
        translation.activate("en")
        language = "en"

    return language


def index(request, lang=None):

    if lang is None:
        if request.user:
            if request.user.is_authenticated:
                lang = request.user.language

    language = activate_language(lang)

    return render(request, 'index.html', {})

# activate user account after clicking on activation link in signup_activation email


def activate(request, uidb64, token, lang=None):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = Profile.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, Profile.DoesNotExist):
        user = None

    if lang == 'fr':
        language = activate_language(lang)
    else:
        language = activate_language('en')

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        auth.login(request, user)
        # return redirect('home')
        return render(request, 'registration/signup_activated.html', {'valid': True})
    else:
        return render(request, 'registration/signup_activated.html', {'valid': False})


def logout(request):
    language = activate_language(request.user.language)
    auth.logout(request)

    if language == 'fr':
        return redirect('/fr/')
    else:
        return redirect('home')


@login_required(login_url='/en/signinup/?next=/profile/')
def profile(request):

    language = translation.activate(request.user.language)
    the_profile = get_object_or_404(Profile, pk=request.user.id)
    #user_data = Grades.objects.get(profile=the_profile)
    user_data = Grades.objects.filter(profile=the_profile).first()
    careers = the_profile.careers.all()
    #programs = the_profile.programs.all()
    salary = None
    if user_data:
        salary = (user_data.summary_salary / 1000)

    return render(request, 'profile.html', {'profile': the_profile, 'data': user_data, 'careers': careers, 'salary': salary})


@login_required(login_url='/en/signinup/?next=/profile/')
def change_user_lang(request, lang=None):

    user = Profile.objects.get(pk=request.user.id)

    if lang == "fr":
        user.language = "fr"
        user.save(update_fields=['language'])
    if lang == "en":
        user.language = "en"
        user.save(update_fields=['language'])

    return redirect('profile')


@login_required(login_url='/en/signinup/?next=/profile/')
def delete_user_data(request, lang=None):

    user = Profile.objects.get(pk=request.user.id)

    user.firstname = "deleted"
    user.lastname = "deleted"
    user.gender = "deleted"
    user.date_birth = None
    user.country = None
    user.is_active = False

    user.save(update_fields=['firstname', 'lastname',
                             'gender', 'date_birth', 'country', 'is_active'])

    return redirect('profile')


def signup(request, lang=None):

    language = activate_language(lang)
    regform = RegistrationForm
    country_id = None

    if request.method == 'POST':

        regform = RegistrationForm(request.POST)
        country_id = request.POST['countries']

        if regform.is_valid():
            new_profile = regform.save(commit=False)
            new_profile.is_active = False
            if country_id:
                country = Country_Diploma.objects.get(pk=country_id)
                new_profile.country = country
            order_id = get_random_string(8).lower()
            new_profile.order_id = order_id
            request.session['order_id'] = order_id
            new_profile.save()

            return redirect(reverse('payment_process'))
            # return render(request, 'registration/signup_confirmation.html', )

    all_countries = Country_Diploma.objects.all()

    if not country_id:
        ip = None
        country = None

        if 'HTTP_X_FORWARDED_FOR' in request.META:
            ip = request.META['HTTP_X_FORWARDED_FOR'].split(",")[0].strip()
            request.META['REMOTE_ADDR'] = ip

        try:
            g = GeoIP2()
            country = g.country(ip)
            diploma = None

            if country:
                # get diploma required by country
                country = Country_Diploma.objects.get(
                    country_code=country["coutry_code"])
                country_id = country["pk"]
        except:
            country_id = None

    return render(request, 'signup.html', {'form': regform, 'country_id': country_id, 'countries': all_countries, })


def signin(request, lang=None):
    language = activate_language(lang)
    request.session['django_language'] = language

    form = LoginForm()

    next = ""
    valid = True

    if request.GET:
        next = request.GET['next']

    if request.method == 'POST':

        form = LoginForm(data=request.POST)

        email = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(username=email, password=password)

        if user is not None:
            auth.login(request, user)
            request.session['just_logged_in'] = 1
            # Check if data for that user exists, if yes than delete grade/course session variables
            try:
                Grades.objects.get(profile__pk=user.pk)
            except Grades.DoesNotExist:
                pass

            if next == "":
                if language == "fr":
                    return redirect('/fr/')
                else:
                    return redirect('home')
            else:
                return redirect(next)

    return render(request, 'signin.html',  {'form': form})


# def error(request):
#    return render(request, "400.html", {} )
