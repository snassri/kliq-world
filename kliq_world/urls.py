from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from journey import views as journey_views
from journey import auth_views
from journey.forms import MyPasswordResetForm

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', journey_views.index, name='home'),
    url(r'^(?P<lang>[a-zA-Z]{2})/$', journey_views.index, name='home'),
    #url(r'^signinup/$', journey_views.signinup, name='signinup'),
    url(r'^(?P<lang>[a-zA-Z]{2})/signup/$', journey_views.signup, name='signup'),
    url(r'^(?P<lang>[a-zA-Z]{2})/signin/$', journey_views.signin, name='signin'),
    url(r'^(?P<lang>[a-zA-Z]{2})/signinup/$', journey_views.signup, name='signinup'),
    url(r'^activate/(?P<lang>[a-zA-Z]{2})/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', journey_views.activate, name='activate'),
    url(r'^logout/$', journey_views.logout, name='logout'),
    url(r'^profile/$', journey_views.profile, name='profile'),
    url(r'^change-lang/(?P<lang>[a-zA-Z]{2})$', journey_views.change_user_lang, name='change_lang'),
    url(r'^change-lang/(?P<lang>[a-zA-Z]{2})$', journey_views.change_user_lang, name='change_lang'),
    url(r'^delete-user-data/$', journey_views.delete_user_data, name='delete_user_data'),
    
    # Password reset links
    url(r'^user/password-reset/$', auth_views.password_reset, {'post_reset_redirect' : '/user/password-reset-done/', 'template_name' : 'registration/password_reset.html', 'password_reset_form' : MyPasswordResetForm, 'from_email': "info@kliq.ca", 'language':'en' }, name="password_reset_en"),
    url(r'^fr/user/password-reset/$', auth_views.password_reset, {'post_reset_redirect' : '/user/password-reset-done/', 'template_name' : 'registration/password_reset.html', 'password_reset_form' : MyPasswordResetForm, 'from_email': "info@kliq.ca", 'language':'fr' }, name="password_reset_fr"),
    url(r'^(?P<language>[a-zA-Z]{2})/user/password-reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', auth_views.password_reset_confirm, {'post_reset_redirect' : '/user/password-done/', 'template_name' : 'registration/password_reset_confirm.html' }, name='password_reset_confirm'),
    url(r'^user/password-reset-done/$', auth_views.password_reset_done, {'template_name': 'registration/password_reset_done.html'},),
    url(r'^user/password-done/$', auth_views.password_reset_complete, {'template_name': 'registration/password_reset_complete.html'}, name='password_done'),
    
    url(r'^process/$', journey_views.payment_process, name='payment_process'),
    url(r'^done/$', journey_views.payment_done, name='payment_done'),
    url(r'^cancelled/$', journey_views.payment_cancelled, name='payment_cancelled'),
    url(r'^paypal/', include('paypal.standard.ipn.urls')),
    url(r'^api/', include(('journey.api_urls', 'journey'), namespace='api')),

    # for testing
    # url(r'^gettoken/', journey_views.get_access_token),
    
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
